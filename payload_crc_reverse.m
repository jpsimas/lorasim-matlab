pkg load communications;
clear all;

crc = [0 0 0 1 1 1 1 1 1 1 0 0 1 1 1 1].';
%%crc_hex = dec2hex(bi2de(crc.'))
dec2hex(de2bi(crc));

data1 = [0:9 0x10:0x19 0x20:0x22];
data1 = de2bi(data1, 8).';
data1 = data1(1:(size(data1, 2)*8)).';

%%data2 = zeros(16, 12);
%%data2(1:(size(data1, 1)*size(data1,2))) = data1(1:(size(data1,1)*size(data1,2)))
%%data1 = mod(sum(data2.'), 2).'
%%clear data2;

%%IBM
pol1 = zeros(17, 1);
pol1([16 15 2 0] + 1) = 1;
crc_test1 = modulo2_polynomial_division([zeros(size(crc)); data1], pol1);

%%CCITT
pol2 = zeros(17, 1);
pol2([16 12 5 0] + 1) = 1;
crc_test2 = modulo2_polynomial_division([zeros(size(crc)); data1], pol2);

%%IBM inverted
pol3 = zeros(17, 1);
pol3(16 - [16 15 2 0] + 1) = 1;
crc_test3 = modulo2_polynomial_division([zeros(size(crc)); data1], pol3);

%%CCITT inverted
pol4 = zeros(17, 1);
pol4(16 - [16 12 5 0] + 1) = 1;
crc_test4 = modulo2_polynomial_division([zeros(size(crc)); data1], pol4);

[crc crc_test1 crc_test2 crc_test3 crc_test4]

%%CRC IS NOT NEGATED - ALL-ZEROS GIVE CRC 0
%%DATA IS NOT NEGATED - ALL-ZEROS GIVE CRC 0
%%CAN BE BOTH NEGATED... - NO in this case zeros will not give zeros
%%IS NOT DONE IN THE RANDOMIZED DATA - all-zeros give crc 0

%%crc = lora_invert_nibbles(crc);
%%data1 = lora_invert_nibbles(data1);
%%crc = crc(end:-1:1);
%%data1 = data1(end:-1:1);

%%multiple = mod([crc; zeros(size(data1))] + [zeros(size(crc)); data1], 2);
multiple = mod([crc; zeros(size(data1, 1) - size(crc, 1), 1)] + data1, 2);

disp("multiple")
multiple.'
find(multiple) - 1
size(multiple)

r = 0;
q = multiple;
while r == 0
  multiple = q;
  [r q] = modulo2_polynomial_division(multiple, [1 1].');
  length(multiple)
end;

multiple.'
size(multiple)
 
pol_inds = [16 15 14 12 9 4 1 0];
pol = zeros(17, 1);
pol(pol_inds + 1) = 1;
crc_calc1 = modulo2_polynomial_division([zeros(length(crc), 1); data1], pol)

[crc crc_calc1]

%{
pol_inds = find(multiple) - 1
pol = zeros(17, 1);
pol(pol_inds + 1) = 1;
crc_calc1 = modulo2_polynomial_division([zeros(size(crc)); data1], pol);

pol_inds = [0 16];
pol = zeros(17, 1);
pol(pol_inds + 1) = 1;
crc_calc1_alt = mod(modulo2_polynomial_division([zeros(size(crc)); data1], pol) + 1,2);

%%[crc crc_calc1 crc_calc1_alt]


if(crc_calc1 ==  crc)
  disp("crc1: ok");
else
  disp("crc1: not ok");
  [crc_calc1 crc_calc1_alt crc]
end;

if(crc_calc1_alt ==  crc)
  disp("crc1_alt: ok");
else
  disp("crc1_alt: not ok");
end;
%}
