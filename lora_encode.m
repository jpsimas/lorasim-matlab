%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function c = lora_encode(bits, G)
  %encodes signal with the selected coding rate

  G_size = size(G);
  
  code_rank = G_size(1);
  code_length = G_size(2);

  bits_grouped = zeros(code_rank, length(bits)/code_rank);
  bits_grouped(1:length(bits)) = bits;

  clear bits;
  
  c = mod(bits_grouped.'*G, 2);
end

