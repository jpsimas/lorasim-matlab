%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_frequency_track2(x, OSF, SF)
  nw = 1;%%3;%%should be at least number of frequencies (i.e. number of different sfs)
  delta = 1;%%OSF;%%normal 1
  
  freq_vect = zeros(size(x, 1) - 1, nw);
  freq = zeros(size(x, 1) - 1, 1);
  freq_down = zeros(size(x, 1) - 1, 1);

  w_vect = zeros(nw, size(x, 1) - 1);
%%  c = zeros(size(x, 1) - 1, 1);
  %%  mu = 1/OSF;
  %%mu = 0.1;%%0.1;
  %%mu = max(0.1, min(1, 2/OSF));

  %%mu = 0.5/OSF;
  mu1 = 0.03/OSF;
  mu2 = 2/OSF;
  
  w1 = ones(nw, 1);
  w2 = ones(nw, 1);

  w2_down = ones(nw, 1);
  
  lambda = 0.5;
  alpha = 0.98;
  a = 2*atanh(lambda);
  %%q = 0;

  M = eye(nw, nw)/nw;
  r = ones(nw, 1);
  lambda_r = zeros(nw, 1);
  lambda_r(1) = 1;
  labmda_r_vect = zeros(size(x, 1) - 1, nw);
  imax = 1;
  flast = 1;
  %%  k = OSF;

  w_c = ones(nw, 1);
  mu_c = mu2;
  
  beta = 1/(OSF^2*2^SF);
  %%w = exp(-1j*2*pi*beta*((length(w)- 1):-1:0).');

  I = (0:(length(x)-1)).';
  chirperino = (exp(1j*pi*beta*I.^2));

  x_down = conj(x).*conj(chirperino);
  x = x.*conj(chirperino);
  
  %%x = x.^OSF;
  
  %%beta = 1/(OSF*2^SF);
  %%k = OSF;
  for i = 1:size(x, 1) - 1 - nw*delta%% - k;
    %%w2 = w2*exp(-1j*2*pi*beta);
    %%w1 = w1*exp(-1j*2*pi*beta);
    
    xi = x(i:delta:(i + delta*nw - 1));
    di = x(i + delta*nw)*nw;

    xi_down = x_down(i:delta:(i + delta*nw - 1));
    di_down = x_down(i + delta*nw)*nw;
%{    
    yi = dot(w2, xi);
    ei = di - yi;

    a = alpha*a + (1 - alpha)*ei*conj(ei);
    lambda = tanh(a/2);

    w1 = w2 + mu1*(di - w2'*xi)'*xi/(dot(xi, xi) + 1e-6);
    w1 = lambda*w1 + (1 - lambda)*w2;
    
    w2 = w1 + mu2*(di - w1'*xi)'*xi/(dot(xi, xi) + 1e-6);
    w2 = (1 - lambda)*w2 + lambda*w1;
%}
   


    


   
    w2 = w2 + mu2*(di - w2'*xi)'*xi/(dot(xi, xi) + 1e-6);
    w2_down = w2_down + mu2*(di_down - w2_down'*xi_down)'*xi_down/(dot(xi_down, xi_down) + 1e-6);


    %%MATRIX APPROACH
    %{
    %%M = M*exp(-1j*2*pi*beta);
    xi = x(i:(i + nw - 1));
    di = x((i + delta):(i + nw + delta - 1));
    
    yi = dot(w2, xi);
    ei = di - yi;    
    
    M = M + mu2*xi*(di - M'*xi)'/(dot(xi, xi) + 1e-6);
    %}
    %%MATRIX APPROACH END;


    
    %%w = w + mu*conj(x(i + 1) - conj(w)*x(i))*x(i)/(x(i)*conj(x(i)));
%%    if x(i) != 0 && x(i + 1) != 0
    %%w = w*exp(-1j*2*pi*beta);
      %%w = (1 - mu)*w + mu*conj(x(i + 1)/x(i));
      %%w = w + mu*(x(i + 1) - w'*x(i))'*x(i);
      %%w = (1 - mu)*w + mu*conj(sign(x(i + 1)/x(i)));
    %%q = (1 - mu)*q + mu*conj(sign(x(i + 2)*conj(x(i + 1))/(x(i + 1)*conj(x(i))))^OSF);
    %%    end;


    %%splitting freqs
    
    %{
    rlast = r;
    %}
    r = roots([1; -w2(end:-1:1)]);
    r_down = roots([1; -w2_down(end:-1:1)]);
%{

    w_c = w_c + mu_c*conj(rlast - conj(w_c).*r).*r/(dot(r, r) + 1e-6);
    
    %%e_r = sign(rlast)*exp(-1j*2*pi*beta) - r;
    e_r = (w_c - exp(-1j*2*pi*beta));
    
    %%lambda_r = e_r.*conj(e_r) + 0.01*abs(abs(r) - 1);
    lambda_r = abs(e_r);
    %%r = eig(M);

    %%maybe select it adaptively somehow(its failing because its selecting the wrong thing in some cases,
%}
    freq_vect(i, :) = r;
%{
    lambda_r_vect(i, :) = lambda_r;
    imaxlast = imax;
    
    [emin, imax] = min(lambda_r);%%max(-abs(abs(r) - 1));%%max(abs(r));%%max(-abs(sign(r.*conj(rlast)) - exp(-1j*2*pi*beta)));%%max(-abs(abs(r) - 1));%%max(-abs(sign(r.*conj(rlast)) - exp(-1j*2*pi*beta)));%%

    if(emin > 1e-1)
      freq(i) = flast;
    end;
    
    freq(i) = r(imax);
    flast = freq(i);
    %}

    w_vect(:, i) = w2;
    %%freq(i) = w2(end);%%sum of the roots
    [emin, imix] = max(abs(r));
    freq(i) = r(imix);

    [emin, imix] = max(abs(r_down));
    freq_down(i) = r_down(imix);    
    
    %%c(i) = q;
  end;

  csvwrite("./misc/w_vect.csv", w_vect);
  
  size(w_vect)
  r_vect = zeros(size(w_vect));
  mu_r = 1e-1;
  p = zeros(nw + 1, 1);

%%  r = r = exp(1j*2*pi*(0:nw-1).'/nw)*exp(1j*2*pi/(8*nw))
  r = (0:nw - 1).' - (nw - 1)/2;%FIX THIS SHOUD NEVER BE 0
  r = exp(1j*pi/4)*r

%%ROOT TRACKER
  %{
  for i = 1:size(w_vect, 2)

    rlast = r;
    p = [1; -w_vect(end:-1:1, i)];
    erri = p - poly(r).';
    erri = erri(2:end);
    
   %% freq_vect(i, :) = roots(p);


    for j = 1:length(r)
      %%r(j) = r(j)*exp(-1j*2*pi*beta*delta);
      r(j) = r(j) - mu_r*deconv(p, [1; -r(j)])'*erri;
    end
    
    if(norm(r) > length(r))
      %%r = length(r)*r/norm(r);
      r = freq_vect(i, :);
    end;
    
    r_vect(:, i) = r;
    %%[emin, imax] = max(-abs(sign(r) - exp(-1j*2*pi*beta)*rlast));
  end;


  rlast = 1;
  for i = 1:size(r_vect, 2)
    r = r_vect(:, i);
    [emin, imin] = min(abs(r - sign(rlast)));
    freq(i) = r(imin);
    rlast = r;
  end;
%}
  
  freq_vect = -angle(freq_vect)/(2*pi);

  figure;
  hold on;
  for i = 1:nw
    plot(freq_vect(1:20000, i), ".");
  end;
  title("freq vect");
  csvwrite("./misc/freq_vect.csv", freq_vect);

  %alpha = 1 - 1e-2;
  %%a = [1, -alpha*exp(-1j*2*pi*beta*delta)]/(1 - alpha);
  %%b = [1];
  
  figure;
  hold on;
  for i = 1:nw
%%    r_vect(i, :) = filter(b, a, r_vect(i, :));
    plot(-arg(r_vect(i, 1:20000))/(2*pi), ".");    
  end;
  title("R vect");
  csvwrite("./misc/r_vect.csv", r_vect);
%{
  figure;
  hold on;
  for i = 1:nw
    plot(log10(lambda_r_vect(1:10000, i)));
  end;
  %}


  %{
  %%DC BLOCKING FILTER
  alpha = 1 - 1e-5;
  a = [1, -alpha];
  b = [alpha, -alpha];
  freq = filter(b, a, freq);

  beta = 1/(OSF^2*2^(SF));
  %%SIGNAL HAS NON-NULL DC-COMPONENT BECAUSE PHASE ONLY GOES FROM
  %% -pi/OSF to pi/OSF... d0es this also change the center freq??
  freq_dc = 2*sin(pi*delta/OSF)/(2*pi*delta/OSF);%%depends on frequency offset...

  %%woks with delta = OSF - 1 and nw = 1, but delta != 1 doesnt really work for nw > 1
  alpha = 1 - 1e-3;
  a = [1, -alpha*exp(-1j*2*pi*beta*delta)]/(1 - alpha);
  b = [1];
  %%freq = filter(b, a, freq);
  %%freq = freq + freq_dc;

  beta1 = delta*beta;
  bw = beta1;
  
%[n, w, betak, ftype] = kaiserord([beta1 - bw, beta1 - bw/2, beta1 + 10*bw/2, beta1 + 10*bw], [1e-1, 1, 1e-1], [1, 1e-3, 1])
  bw_low = beta1;
  bw = beta1*50;
  [n, w, betak, ftype] = kaiserord ([0, beta1 - bw_low/2, beta1 + bw/2, beta1 + 1.1*bw/2], [1e-1, 1, 1e-1], [1, 1e-3, 1]);

  a = [1];
  b = fir1 (n, w, kaiser (n+1, betak), ftype, "noscale");
  
%%  freq = filter(b, [1], freq);
  
  %%FIR band-stop @ freq -> NORMALIZE PASS BAND!!!
  beta2 = 1/(OSF^2*2^(SF + 1));
  alpha = 1;
  a = [1];
  b = [1, -alpha*exp(-1j*2*pi*beta2*delta)]/(1 - alpha*exp(-1j*2*pi*(beta2 - beta)*delta));
%%  freq = filter(b, a, freq);

  %%FIR band-pass
  beta2 = 1/(OSF^2*2^(SF));
  alpha = 1;
  a = [(1 + alpha)];
  b = [alpha, alpha*exp(-1j*2*pi*beta2*delta)];
%%  freq = filter(b, a, freq);

  
  alpha = 0.999;
  a = [1, -alpha*exp(-1j*2*pi*beta*delta)];
  b = [(1 - alpha)];
%%  freq = filter(b, a, freq);
  
%}

%%  I = (0:(size(freq, 1) - 1)).';
%%  chirperino = (exp(1j*2*pi*beta*I));
  %%  freq = freq.*chirperino;
  
  %{
  nwf = 10;
  wf = zeros(nwf, 1);
  deltaf = OSF;
  y = zeros(size(freq, 1) - 1 - nwf, 1);
  err = zeros(size(freq, 1) - 1 - nwf, 1);
  mu = 1e-5;


  for i = 1:size(freq, 1) - 1 - nwf - deltaf
    xi = freq(i:(i + nwf - 1));
    di = freq(i + nwf - 1 + deltaf)*exp(1j*pi*delta*beta*deltaf);
    
    y(i) = dot(wf, xi);
    err(i) = di - y(i);

    wf = wf + mu*conj(err(i))*xi;
  end;

  figure;
  freqz(wf, [1], 1e5, "whole");

  
  figure;
  hold on;
  plot(-angle(freq)/(2*pi));
  plot(-angle(y)/(2*pi));
    
  clear freq;
  freq = y;
  clear y;

  
  figure;
  plot(20*log10(abs(err)));
  title("MSE (dB)");
  %}  
  figure;
  plot(20*log10(abs(fft(freq))));
  title("EBIN (dB)");

  figure;
  plot(20*log10(abs(freq)));
  title("power (dB)");
  
  %%M
  figure;
  %%plot(arg(c));
  title("BENIS");
  %%freq = freq.*exp(-1j*2*pi*beta*(0:length(freq)-1).');
  freq = -angle(freq)/(2*pi);
  freq_down = -angle(freq_down)/(2*pi);
  
  I = (0:(size(freq, 1) - 1)).';
  freq = mod(freq + beta*I, 1);%%delay of the frequecy signal relative to the input
  
  freq_down = mod(freq_down + beta*I, 1);
  freq_down = mod(-freq_down, 1);
  
  freq = mod(freq + 0.5, 1) - 0.5;
  freq_down = mod(freq_down + 0.5, 1) - 0.5;

  figure;
  hold on;
  plot(freq);
  plot(freq_down);
end;
