%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function s = lora_chirp_mod(symbols, OSF, SF, phi0)
  c = lora_chirp(1, SF, 0, OSF);

  n_syms = length(symbols);
  symbol_length = 2^SF*OSF;%samples per symbol/chirp
  
% chirped spread spectrum modulation
  n_s = n_syms*symbol_length;
  s = zeros(n_s, 1);
  
  for i = 1:n_syms
    s((i-1)*symbol_length + (1:symbol_length)) =  exp(1i*phi0)*conj(c(1 + symbols(i)*OSF))*c(1 + mod(symbols(i)*OSF + (0:symbol_length-1), symbol_length));
  end;
end
