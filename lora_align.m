%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_align(freq, SF, OSF, syncword_number)

%{
  filter_n = 20;
  filter_length = 2*filter_n + 1;
  I = -filter_n:filter_n;

  bw = 5/OSF;

  filter_b = (sin(pi*I/filter_n)./(pi*I/filter_n)).*(-bw*cos(pi*I*bw) + 1/pi*1./I.*sin(pi*I*bw))./I;%%low-pass differentiator + rectangular window
  filter_b = filter_b.';
  
  filter_b(filter_n + 1) = 0;


  filter_b = -filter_b/dot(filter_b, I);
  
%  figure;
%  f = linspace(-0.5, 0.5, 100);
%  h = freqz(filter_b, [1], 2*pi*f);
%  plot(f, abs(h));

  x_derivative = filter(filter_b, [1], [x; zeros(filter_n, 1)]);
  x_derivative = x_derivative(filter_length:end);
  freq = [imag(x_derivative.*conj(x((filter_n + 1):(length(x)))))];
  
  clear x_derivative;


%}


  
  clear x;
  
  figure;
  plot(freq);
  
  symbol_length = 2^SF*OSF;

  upchirp = lora_chirp_freq(SF, OSF);
  %%minimal preamble (indicates start of frame)
  %%TODO: implement syncword as a parameter

  %%sync_word = [upchirp; upchirp];
  %%sync_word = [upchirp(1 + mod(24*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(32*OSF + (0:symbol_length-1), symbol_length))];

  %%syncword number: higher nibble represents first symbol and lower, the second one
  
  syncword_number = mod(round(syncword_number), 0x100);%truncate to a byte
  sync_sym1 = 2^(SF - 5)*floor(syncword_number/0x10);
  sync_sym2 = 2^(SF - 5)*mod(syncword_number, 0x10);
  
  sync_word = [upchirp(1 + mod(sync_sym1*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(sync_sym2*OSF + (0:symbol_length-1), symbol_length))];

  section_length = (symbol_length/4 - OSF);
  
  minimal_preamble = [sync_word; -upchirp; -upchirp; -upchirp((1:section_length))];

  minimal_preamble = minimal_preamble - mean(minimal_preamble);

  minimal_preamble = minimal_preamble/norm(minimal_preamble);
  
  preamble_length = length(minimal_preamble);
  
  ind = 1;
  found = false;
  
  M_max = 0;

  p = OSF;

  corri = zeros(size(freq), 1);
  
  i = 1;
  ind_offset = 0;
  while i <= (length(freq) - preamble_length + 1)
    freqi = freq((i):(i + preamble_length - 1));
    M = dot(freqi, minimal_preamble)/(norm(freqi - mean(freqi)));
    corri(i) = M;
%%    M = M*conj(M);%%%M is real and should be positive for detection

    if ~found
      if M >= 0.9
	ind = ind - (i - p - 1);
	freq = freq((i - p):end);
	i = 1;
	p = 1;
	found = true
      end;
    else
      if M < 0.7
	break;
      end;
    end;

    if M >= M_max
      M_max = M;
      ind = i;
    end;
    i = i + p;
  end
  
  if ~found
    disp("ALIGN ERROR: PREAMBLE NOT FOUND");
  end;
  M_max

  figure;
  hold on;
  plot(corri(1:(ind + preamble_length - 1)));
  plot(freq(1:(ind + preamble_length - 1)));
  legend("Correlation", "Normalized Instantaneous Frequency");
  ylim([-1 1]);
  xlabel("Sample");
  title("Frequency Signal Correlation");
  
  figure;
  hold on;
  mean_pre = mean(freq(ind:(ind + preamble_length - 1)));
  plot((freq(ind:(ind + preamble_length - 1)) - mean_pre)/norm(freq(ind:(ind + preamble_length - 1)) - mean_pre));

  freq = freq - mean_pre;
  
  plot(minimal_preamble);
  title("Preamble Samples");
  xlabel("Samples");
  ylabel("Normalized Frequency");
  legend("Expected Preamble", "Received Preamble");
  ylim([-0.5 0.5]);

  freq = freq((ind + preamble_length):end);

end
 
