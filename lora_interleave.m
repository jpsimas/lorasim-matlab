%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function y = lora_interleave(encoded_signal, SF)
  signal_dims = size(encoded_signal);
  code_length = signal_dims(2);
  
  interleaver_size = code_length*SF;

  n_codedbits = signal_dims(1)*code_length;
  
  n_blocks = n_codedbits/interleaver_size;
  n_syms = n_codedbits/SF;
  
  y = zeros(n_syms,SF);
  for i = 0:(n_blocks - 1)
    for k = 0:(code_length - 1)
      for m = 0:(SF - 1)
	y(i*code_length + k + 1, m + 1) = encoded_signal(i*SF + mod(m + k, SF) + 1, k + 1);
      end
    end
  end
end
