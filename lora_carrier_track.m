%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function x_baseband = lora_carrier_track(x, OSF, SF)
  nw = 4;
  mu = 0.1;

  w = ones(nw, 1)/nw;
  
  for i = 1:size(x, 1) - 1 - nw%% - k;
    xi = x(i:(i + nw - 1));
    di = x(i + nw);
    
    yi = dot(w, xi);
    ei = di - yi;

    w = w + mu*(di - w'*xi)'*xi/(dot(xi, xi) + 1e-6);
    
    p = roots([1; -w(end:-1:1)]);
    r = roots(p);
    [emin, imin] = min(abs(abs(r) - 1))
    freq(i) = r(imin);
  end;
  figure
  plot(-arg(freq)/(2*pi));
  
  x_baseband = x;
end;
