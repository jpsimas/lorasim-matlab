#+TITLE: Software-Defined Radio Implementation of a LoRa Detector and Transceiver
#+SUBTITLE: Thesis Abstract
#+AUTHOR: João Pedro de Omena Simas
#+OPTIONS: toc:nil
#+DATE:

The number of applications of low-power wide-area networs (LPWANs) has
been growing quite considerably in the past few years and so has the
number of protocol stacks. Despite this fact, there's still no fully
open LPWAN protocol stack available to the public, which limits the
flexibility and ease-of-integration of the existing ones. The closest
to being fully open is LoRa, however only its medium acess control
(MAC) layer, know as LoRaWAN, is open and its physical and logical
link control layers, also known as LoRa PHY, are still only partially
understood.

In this thesis, the essential missing aspects of LoRa PHY are not only
reverse-engineered, but also a new design of the transceiver and its
sub-components is proposed and implemented in a modular and flexible
way using GNU Radio.

Finally, some examples of applications of both the transceiver and its
components, which are made to be run in a simple setup using cheap
widely available of-the-shelf hardware, are given to show how the
library can be used and extended.
