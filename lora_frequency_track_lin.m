%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_frequency_track_lin(x, OSF, SF)
  %%  mu = 1/OSF;
  %%mu = 0.1;%%0.1;
  %%mu = max(0.1, min(1, 2/OSF));
  mu = (2/OSF);

  w = 0;

  %%  k = OSF;

  nw = 1;

  freq = zeros(size(x, 1) - nw, 1);

  beta = 1/(OSF^2*2^SF);
  
  I = (0:(length(x)-1)).';
  chirperino = (exp(1j*pi*beta*I.^2));

  x = x.*conj(chirperino);
  
  for i = 1:size(x, 1) - nw
    %%w = mod(w + 2*pi*beta + pi, 2*pi) - pi;

    err = mean(arg(exp(1j*w).*conj(x((i + 1):(i + nw)).*conj(x(i:(i + nw - 1))))));
    
    w = mod(w - mu*err + pi, 2*pi) - pi;
    freq(i) = w;

  end;

  freq = mod(freq  + 2*pi*beta*(I((1 + nw):end)) + pi, 2*pi) - pi;
  
  freq = freq/(2*pi);
end;
