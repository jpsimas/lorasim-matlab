%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_align_complex(freq, SF, OSF, syncword_number)
  
  figure;
  plot(freq);
  
  symbol_length = 2^SF*OSF;

  upchirp = lora_chirp_freq(SF, OSF);
  %%syncword number: higher nibble represents first symbol and lower, the second one
  
  syncword_number = mod(round(syncword_number), 0x100);%truncate to a byte
  sync_sym1 = 2^(SF - 5)*floor(syncword_number/0x10);
  sync_sym2 = 2^(SF - 5)*mod(syncword_number, 0x10);
  
  sync_word = [upchirp(1 + mod(sync_sym1*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(sync_sym2*OSF + (0:symbol_length-1), symbol_length))];
  
  section_length = (symbol_length/4 - OSF);
  downchirps = [-upchirp; -upchirp; -upchirp((1:section_length))];

  downchirps = downchirps - mean(downchirps);
  
  minimal_preamble = [sync_word; 1j*downchirps];
  
  %%minimal_preamble = minimal_preamble/norm(minimal_preamble);
  sync_word = sync_word/norm(minimal_preamble);
  downchirps = downchirps/norm(minimal_preamble);
  
  preamble_length = length(minimal_preamble);
  
  ind = 1;
  found = false;
  
  M_max = 0;

  p = OSF;
  
  i = 1;
  ind_offset = 0;
  while i <= (length(freq) - preamble_length + 1)
    freqi1 = real(freq((i):(i + length(sync_word) - 1)));
    freqi2 = imag(freq((i + length(sync_word)):(i + preamble_length - 1)));
    freqi1 = freqi1 - mean(freqi1);
    freqi2 = freqi2 - mean(freqi2);
    M = (dot(freqi1, sync_word) + dot(freqi2, downchirps))/norm([freqi1; freqi2]);
%%    M = M*conj(M);%%%M is real and should be positive for detection

    if ~found
      if M >= 0.92
	ind = ind - (i - p - 1);
	freq = freq((i - p):end);
	i = 1;
	p = 1;
	found = true
      end;
    else
      if M < 0.7
	break;
      end;
    end;

    if M >= M_max
      M_max = M;
      ind = i;
    end;
    i = i + p;
  end
  
  if ~found
    disp("ALIGN ERROR: PREAMBLE NOT FOUND");
  end;
  M_max
  
  mean_pre = mean(real(freq(ind:(ind + length(sync_word) - 1))));
    
  freq = freq - mean_pre;

  figure;
  hold on;
  plot(real(freq(ind:(ind + length(sync_word)- 1))));
  plot(sync_word);

  figure;
  hold on;
  mean_pre = mean(imag(freq((ind + length(sync_word)):(ind + preamble_length - 1))));
  plot(imag(freq((ind + length(sync_word)):(ind + preamble_length - 1))) - mean_pre);
  plot(real(freq((ind + length(sync_word)):(ind + preamble_length - 1))));
  plot(downchirps);

  freq = real(freq((ind + preamble_length):end));
  
  
end
 
