%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function s = lora_modulate(symbols, OSF, SF, syncword_number, freq_offset_relative, phi0)
  upchirp = lora_chirp_freq(SF, OSF);

  n_syms = length(symbols);
  symbol_length = 2^SF*OSF;%samples per symbol/chirp
  
% chirped spread spectrum modulation
  n_s = n_syms*symbol_length;
  s = zeros(n_s, 1);
  
  for i = 1:n_syms
    s((i-1)*symbol_length + (1:symbol_length)) = upchirp(1 + mod(symbols(i)*OSF + (0:symbol_length-1), symbol_length));
  end;

  %%syncword number: higher nibble represents first symbol and lower, the second one
  syncword_number = mod(round(syncword_number), 0x100);%truncate to a byte
  sync_sym1 = 2^(SF - 5)*floor(syncword_number/0x10);
  sync_sym2 = 2^(SF - 5)*mod(syncword_number, 0x10);
  
  %% append upchirps + sync word + frame sync symbols (for the moment, minimal length asuming sync word uc uc ie 2 upchirps 2 and 1/4 downchirps)
  sync_word = [upchirp(1 + mod(sync_sym1*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(sync_sym2*OSF + (0:symbol_length-1), symbol_length))];
  s = [upchirp; upchirp; upchirp; upchirp; upchirp; sync_word; -upchirp; -upchirp; -upchirp((1:(length(upchirp)/4 - OSF))); s];
  
  s = s + freq_offset_relative*1/OSF;
  
  s = exp(1i*(2*pi*filter([1], [1 -1], s)));
  s = conj(s(1))*exp(1i*phi0)*s;
end
