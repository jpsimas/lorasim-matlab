%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function bits = lora_rx(r, OSF, SF, syncword_number, implicit_mode, low_data_rate, no_align, payload_length_implicit, CR_implicit, payload_crc_present_implicit)
  pkg load communications;

  figure;
  t = (0:(length(r) - 1)).';
  %% specgram(r.*exp(1j*2*pi*(1/OSF/2)*t), 8*OSF, 1);
  pkg load ltfat;
  sgram(r.*exp(1j*2*pi*(1/OSF/2)*t), 0.5);
  title("Received Signal Spectrogram");
  xlabel("Sample");
  ylabel("Normalized Frequency");
  
  %% number of samples for a chirp
  symbol_length = 2^SF*OSF;

  %%r = lora_chirp_select(r, OSF, SF);

  use_new_demod = false;

  if(~use_new_demod)
    r = lora_frequency_track(r, OSF, SF);
  %%r = lora_frequency_track_lin(r, OSF, SF);
    %%r = lora_frequency_track2(r, OSF, SF);
  else
  %%r = lora_calc_frequency(r, OSF, SF);
    r = lora_calc_frequecy_chirp_window(r, OSF, SF);
  %%r = lora_calc_frequency_wiegner(r, OSF, SF);
  %%r = lora_frequency_track(r, OSF, SF);
  %%r = lora_frequency_track_lin(r, OSF, SF);
  end;
  
  figure;
  plot(r);
  title("Instantaneous Frequecy Estimate");
  xlabel("Sample");
  ylabel("Normalized Frequency");
  ylim([-0.5 0.5]);
  
  %%r = lora_calc_frequency2(r, OSF, SF);
  %%r = lora_frequency_track_new(r, OSF, SF);
  %%r = lora_frequency_track_new2(r, OSF, SF);
  

  
  %% align signal
  if ~no_align
    if(~use_new_demod)
      r = lora_align(r, SF, OSF, syncword_number);
    %%r = lora_align_complex(r, SF, OSF, syncword_number);
      %%r = lora_align_new(r, r2, is_downchirp, SF, OSF, syncword_number);
      else
    %%r = lora_align_demod_lazy(r, SF, OSF, syncword_number);
	r = lora_align_demod(r, SF, OSF, syncword_number);
    end;
  end;

  figure;
  plot(mod(r + 0.5, 1) - 0.5);
  title("Aligned Signal");
  xlabel("Time");
  xlabel("Frequency");
  ylim([-0.5 0.5]);

  %%return;
  
  if low_data_rate
    SF_deinterleaver = SF - 2;
  else
    SF_deinterleaver =  SF;
  end;

  time_offset = 0;
  
  if ~implicit_mode
%%%DECODE HEADER
    %% split signal into the header signal (r_header) and the playload signal (r)
    CR_header = 4;
    code_rank = 4;
    code_length_header = code_rank + CR_header;
    if(~use_new_demod)
      r_header = r(1:(8*symbol_length + OSF));%%add OSF extra samples at the end to ensure re-alignment works
    end;
    
    %% fft based demodulation (assumes perfect
    %% synchronization)
    %%symbols_header = lora_demodulate_new(r_header, SF, OSF)

    %%symbols_header = lora_demodulate_level(r_header, SF, OSF)
    if(~use_new_demod)
      symbols_header = lora_demodulate(r_header, SF, OSF)
    else
      symbols_header = r(1:8)
    end;
    symbols_header = round(symbols_header/4);
    
    %%[symbols_header, time_offset] = lora_demodulate(r_header, SF - 2, 4*OSF)
    if(~use_new_demod)
      r = r((8*symbol_length + 1):end);
    else
    %%r = r((8*symbol_length + 1 + round(time_offset)):end);
      r = r(9:end);
    end;
    
    %%add check here to see if symbols were really sent with SF-2 interleaver
    S_header = gray_encode(symbols_header, SF-2)
    
    C_header = lora_deinterleave(S_header, CR_header)

    %%Split header codewords in header
    C_payload_in_header = C_header(6:end, :);
    C_header = C_header(1:5, :);
    
    %%generate code matrices
    [G H] = lora_get_code_matrices(CR_header);
    %%coset leaders LUT
    [cl cl_found] = get_coset_leaders_lut(H);

    bits_header = lora_decode(C_header, H, G, cl, cl_found)
 
   %%invert bit order of the nibbles
%%    bits_header = lora_invert_nibbles(bits_header);
    
    %%get data from header
    %% payload_length = bi2de(bits_header(1:8).', 'left-msb')%%length in bytes
    payload_length = bi2de(bits_header([5:8 1:4]).')%%length in bytes
    %%CR = bi2de(bits_header(9:11).', 'left-msb')
    CR = bi2de(bits_header(10:12).')
    
    if CR > 4 || CR == 0
      disp("Invalid CR, setting it to 4");
      CR = 4;
    end;
    
    %%payload_crc_present = bits_header(12)
    payload_crc_present = bits_header(9)
    %%header_checksum = bi2de(bits_header(16:20).')
    header_checksum = bi2de(bits_header([17:20 13:16]).')
    
    header_checksum_calculated = bi2de(lora_header_checksum(bits_header(1:12)).')
    
    if header_checksum == header_checksum_calculated
      disp("Header checksum pass");
    else
      disp("Header checksum fail");
      return;
    end;

%%%DECODE HEADER END
    %% get just the part of the signal with the payload, given that the length is now known

    if payload_crc_present
      payload_crc_length = 2;
      n_payload_symbols_outside_header = ceil((payload_length + payload_crc_length) * 8 * (CR + 4)/CR / SF_deinterleaver);
    else
      payload_crc_length = 0;
      n_payload_symbols_outside_header = ceil((payload_length*8 - 4*(SF - 7)) * (CR + 4)/CR / SF_deinterleaver);
    end;
    %% round up so there are a whole number of codewords
    n_payload_symbols_outside_header = (CR + 4)*ceil(n_payload_symbols_outside_header/(CR + 4));
    if(~use_new_demod)
      r = r(1:(n_payload_symbols_outside_header*symbol_length + symbol_length - 1));%%add OSF extra samples at the end to ensure re-alignment works
    else
      r = r(1:n_payload_symbols_outside_header);
    end;
  else
    payload_length = payload_length_implicit;
    CR = CR_implicit;
    payload_crc_present = payload_crc_present_implicit;
    
    %% truncate received signal to have a length multiple of the symbol length
    code_rank = 4;
    code_length = code_rank + CR;
    %%    r = r(1:(floor(length(r)/(symbol_length*code_length))*(symbol_length*code_length)));
    if payload_crc_present
      payload_crc_length = 2;
    else
      payload_crc_length = 0;
    end;

    n_payload_symbols = ceil((payload_length + payload_crc_length) * 8 * (CR + 4)/CR / SF_deinterleaver);

    n_payload_symbols = (CR + 4)*ceil(n_payload_symbols/(CR + 4));

    if(~use_new_demod)
      r = r(1:(n_payload_symbols*symbol_length + OSF));
    else
      r = r(1:n_payload_symbols);
    end;
  end;
  %% fft based demodulation (assumes perfect
  %% synchronization)

  if(~use_new_demod)
    symbols = lora_demodulate(r, SF, OSF)
    %%symbols = lora_demodulate_level(r, SF, OSF)
    %%symbols = lora_demodulate_new(r, SF, OSF)
  else
    symbols = r
  end;
  
  clear r;
  
  if low_data_rate
    symbols = round(symbols/4);
  end;
  
  %% gray mapping
  S = gray_encode(symbols, SF_deinterleaver);
  
  %% deinterleaving
  C = lora_deinterleave(S, CR);
  
  if ~implicit_mode
    C = [C_payload_in_header(:,1:(4 + CR)); C];
  end;

  %% Hamming decoder
  %%generate code matrices
  [G H] = lora_get_code_matrices(CR);
  %%coset leaders LUT
  [cl cl_found] = get_coset_leaders_lut(H);
  
  bits = lora_decode(C, H, cl, cl_found);

  bits = bits(1:(8*(payload_length + payload_crc_length)));

  %%get crc bits
  if payload_crc_present
    bits_crc = bits((8*payload_length + 1):end);
    bits_crc = lora_invert_endianness(bits_crc);
  end;

  %%get data bits
  bits = bits(1:(8*payload_length));
  
  %%%DE-RANDOMIZE DATA
  bits = lora_randomize(bits);
  
  if payload_crc_present
    disp("BITS/BITSCRC");
    bits.'
    bits_crc.'
    
    %%CRC
    bits_crc_calc = lora_payload_crc(bits)
    
    if bits_crc == bits_crc_calc
      disp("Payload CRC pass");
    else
      disp("Payload CRC fail");
    end;
  end;
end
