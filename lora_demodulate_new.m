%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function symbols = lora_demodulate_new(freq, SF, OSF)
  symbol_length = 2^SF*OSF;

  n_sym = floor(length(freq)/symbol_length);
  symbols = zeros(n_sym, 1);

  upchirp = lora_chirp_freq(SF, OSF);
  offset = 0;
  for sym = 1:n_sym
    if sym == n_sym && n_sym*symbol_length + round(offset) > length(freq)
      disp("lora_demodulate: not enough samples to correct for re-alignment");
      offset = 0;
    end;
    
    symi = freq((sym-1)*symbol_length+(1:symbol_length));

    %%figure;
    %%plot(2^SF*symi);
    
    i = mean(symi(symbol_length/2 + ((-symbol_length/16):(symbol_length/16 - 1))))*2^SF - offset;

    %%re-align
    ind_error = i - round(i);
    %%offset = offset + 0.5*ind_error
   
    symbols(sym) = mod(round(i), 2^SF);
  end;
end
