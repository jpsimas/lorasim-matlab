%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function x = gray_decode(bits)
  pkg load communications;
  size_bits = size(bits);
  n_bits = size_bits(2);
  for i = 1:(n_bits - 1)
    bits(:, n_bits - i) = mod(bits(:, n_bits - i) + bits(:, n_bits - i + 1), 2);
  end;
  
  x = bi2de(bits);
end
