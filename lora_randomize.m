%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function bits = lora_randomize(bits)
  state = ones(8, 1);
  for i = 1:floor(size(bits, 1)/8)
    %%    bits((8*(i - 1) + 1):(8*i)) = mod(bits((8*(i - 1) + 1):(8*i)) + state([4:-1:1 8:-1:5]), 2);
    bits((8*(i - 1) + 1):(8*i)) = mod(bits((8*(i - 1) + 1):(8*i)) + state, 2);
    state = [mod(sum(state([4 5 6 8])), 2); state(1:7)];
  end

  q = floor(size(bits, 1)/8);
  bits((8*q + 1):size(bits, 1)) = mod(bits((8*q + 1):size(bits,1)) + state(1:(size(bits,1) - 8*q)), 2);
end
