clear all;
close all;

pkg load communications;

a1 = lora_get_whitening_sequence(6, false, false);
a2 = lora_get_whitening_sequence(6, true, false);

len = min(length(a1), length(a2));

b = bitxor(a1(1:len), a2(1:len));

b = de2bi(b).';
sizeb = size(b);

b = b(sizeb(1):-1:1,:);

b = b(1:(sizeb(1)*sizeb(2)));
b = b.';
%% de2bi(b(1:64))

size(b)

n_w = 49;

known = zeros(n_w, 1);
w = zeros(n_w, 1);

all_known = false;
fail = false;


%%for i = 1:(length(b) - n_w);
i = 1;
while(i <= length(b) - n_w);
  if(sum(known) ~= length(known))
    x = (~known)&b(i:(i + n_w - 1));
    cnt = sum(x);
    if(cnt == 1)
      ind = find(x);
      if(~known(ind))
	known(ind) = 1;
	w(ind) = mod(b(i + n_w) + dot(w, b(i:(i + n_w - 1))),2);
      else
	if(w(ind) ~= mod(b(i + n_w) + dot(w, b(i:(i + n_w - 1))),2))
	  fail = true;
	end;
      end;
    end;
  end;
  
  test = mod(b(i + n_w) + dot(w, b(i:(i + n_w - 1))), 2);
  if(test || fail)
    fail = false;
    disp("extending w");
    w(2:(n_w + 1)) = w;
    w(1) = 0;
    known(2:(n_w + 1)) = known;
    known(1) = 0;
    n_w = n_w + 1;
    i = i - 1;
  end;

  i = i + 1;
end;

if(fail || sum(known) ~= length(known))
  disp("FAIL");
else
  disp("OK");
end;

%%TEST again

for i = 1:(length(b) - n_w);
  test = mod(b(i + n_w) + dot(w, b(i:(i + n_w - 1))), 2);
  if(test)
    disp("FAIL");
    i
    break;
  end;
end;
