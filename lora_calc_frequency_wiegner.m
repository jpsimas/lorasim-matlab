%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_calc_frequency_wiegner(x, OSF, SF)
  freq = zeros(size(x, 1), 1);
  freq2 = zeros(size(x, 1), 1);

  symbolLength = 2^SF * OSF;
  
  beta = 1/(symbolLength*OSF);
  k0 = 1;
  n_dft = symbolLength;
  tau = k0/(beta*n_dft);
  DFT = zeros(n_dft, 1);

  x = x((tau + 1):end).*conj(x(1:end-tau));
  
  figure;
  specgram(x, n_dft);
  
  alpha = 0;
  
  exponents = (1 - alpha)*exp(1j*2*pi*(0:(size(DFT, 1) - 1)).'/(size(DFT, 1)));
  exponentsN = exponents.^n_dft;
  
  
  for i = 1:min(n_dft, size(x, 1))
    DFT = exponents.*DFT + x(i);
    freq(i) = DFT(1 + k0);
    
    freq2(i) = DFT(1 + (n_dft - k0));
  end;
  
  for i = (n_dft + 1):size(x, 1)
    DFT = exponents.*DFT + x(i) - x(i - n_dft).*exponentsN;
    freq(i) = DFT(1 + k0);
    
    freq2(i) = DFT(1 + (n_dft - k0));
  end;

  is_downchirp = (abs(freq2) > abs(freq));
  freq = (~is_downchirp).*freq + is_downchirp.*freq2;

  freq = -freq;
  
  figure;
  hold on;
  plot(arg(freq));
  plot(is_downchirp);
  
  figure;
  hold on;
  plot(abs(freq));
  plot(abs(freq2));
  title("amplitudes");

  freq = arg(freq)/(2*pi);
end;
