%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_align_new(freq, freq2, is_downchirp, SF, OSF, syncword_number)

  symbol_length = 2^SF*OSF;
  
  figure;
  plot(freq);
  
 %%syncword number: higher nibble represents first symbol and lower, the second one
  syncword_number = mod(round(syncword_number), 0x100);%truncate to a byte
  sync_sym1 = 2^(SF - 5)*floor(syncword_number/0x10)
  sync_sym2 = 2^(SF - 5)*mod(syncword_number, 0x10)

  upchirp = lora_chirp_freq(SF, OSF);
  
  sync_word = [upchirp(1 + mod(sync_sym1*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(sync_sym2*OSF + (0:symbol_length-1), symbol_length))];

  minimal_preamble = [upchirp; upchirp; upchirp; upchirp; upchirp; sync_word];

  downchirps = zeros(2*symbol_length + symbol_length/4 - OSF, 1);
  downchirps_length = length(downchirps);
  %%THIS IS WRONG

  beta = 1/(OSF^2*2^SF);
  I = (0:(length(minimal_preamble)-1)).';
  %%minimal_preamble = mod(minimal_preamble - beta*I, 1);%%with this works, but has ab ofset
  minimal_preamble = mod(minimal_preamble - beta*I - 1/(2*OSF), 1);

  preamble_length = length(minimal_preamble);
  
  figure;
  freq = mod(OSF*freq, 1);
  freq2 = mod(OSF*freq2, 1);
  plot(2^SF*freq);
  title("UNALIGNED SIG");
  
  figure;
  minimal_preamble = mod(OSF*minimal_preamble, 1);
  plot(2^SF*minimal_preamble);
  title("PREAMBLE");
  
  ind = 1;
  found = false;
  
  M_max = 0;
  absMMax = 0;
 
  
  p = OSF;

  size(freq)
  size(is_downchirp)

  
  i = 1;
  ind_offset = 0;
  while i <= (length(freq) - preamble_length - downchirps_length + 1)
    freqi = freq((i):(i + preamble_length - 1));
    M = dot(exp(1j*2*pi*minimal_preamble), exp(1j*2*pi*freqi))/preamble_length;

    absM = abs(M)^2;
    %%erri = mod(minimal_preamble - freqi, 1);
    %%M = 1 - dot(erri, erri)/preamble_length;
    
    if ~found
      if absM > 0.99
	absM
	%%if abs(M) >= 0.4
	ind = ind - (i - p - 1);
	freq = freq((i - p):end);	
	freq2 = freq2((i - p):end);
	is_downchirp = is_downchirp((i - p):end);
	i = 1;
	p = 1;
	found = true
      end;
    else
      if absM < 0.8
	break;
      end;
    end;

    if absM >= absMMax
      absMMax = absM;
      M_max = M;
      ind = i;
    end;
    i = i + p;
  end
  
  if ~found
    disp("ALIGN ERROR: PREAMBLE NOT FOUND");
  end;
  M_max
  absMMax
  
  %%freq = freq*sign(M_max);
  
  figure;
  hold on;
  %%mean_pre = mean(freq(ind:(ind + preamble_length - 1)));
  %%plot((freq(ind:(ind + preamble_length - 1)) - mean_pre)/norm(freq(ind:(ind + preamble_length - 1)) - mean_pre));

  delta_f = arg(M_max)/(2*pi)
  %%delta_f = mean(mod(freq(ind:(ind + preamble_length - 1)) - minimal_preamble, 1))
  
  freq = mod(freq, 1);
  freq2 = mod(-freq2, 1);
  %%  freq = mod(freq - 90/(2^SF), 1);%SUPER BODGE

  plot(2^SF*freq(ind:(ind + preamble_length - 1)));
  plot(2^SF*minimal_preamble);

  figure;
  plot(2^SF*freq2);
  
  figure;
  hold on;
  plot(2^SF*freq2((ind + preamble_length):(ind + preamble_length + downchirps_length - 1)));
  plot(2^SF*downchirps);
  %%plot(arg(exp(1j*2*pi*freq(ind:(ind + preamble_length - 1))))/(2*pi));

  I = (0:(length(freq)-1)).';
  %%freq = mod(freq + beta*OSF*I, 1);
  %%freq2 = mod(freq2 - beta*OSF*I, 1);
  
  freq = (~is_downchirp).*freq + is_downchirp.*freq2;

  figure;
  hold on;
  plot(freq*2^SF);
  plot(is_downchirp*2^SF);
  
  %%freq = freq((ind + preamble_length ):end);
  freq = freq((ind + preamble_length + downchirps_length):end);
%%  freq = mod(freq + (2^(SF - 2) - 1)*2^(-SF), 1);%%add frequency step caused by downchirps



  figure;
  plot(freq*2^SF);
  title("ALIGNED");
end
 
