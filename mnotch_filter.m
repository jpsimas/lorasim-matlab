format long e;

close all;
clear all;
OSF = 8;
SF1 = 7;
SF2= 8;

delta = OSF;

beta1 = delta/(OSF^2*2^SF1)
beta2 = delta/(OSF^2*2^SF2)
%{
alpha = 1 - 1e-4;
a = [1, -alpha*exp(1j*2*pi*beta1)];
b = [(1 - alpha)];

figure;
freqz(b, a, 1e5, "whole")

alpha = 1 - 1e-4;
a = [1, -alpha*exp(1j*2*pi*beta2)];
b = [(1 - alpha)];

figure;
freqz(b, a, 1e5, "whole")
%}

%{
alpha = 1;
a = [1];
b = [1, -alpha*exp(1j*2*pi*beta1)];
b = b/(1 + alpha);

b = conv(b, b);

figure;
freqz(b, a, 1e5, "whole")
%}


%{
a = [1, -alpha*exp(-1j*2*pi*beta1)];
b = [alpha, -alpha*exp(-1j*2*pi*beta1)];
figure;
freqz(b, a, 1e5, "whole")
%}

%{
alpha = 1 - 1e-4;
a = [1];
b = [1, -alpha*exp(1j*2*pi*beta1)]/(1 + alpha);

figure;
freqz(b, a, 1e5, "whole")
%}

%{
alpha = 1 - 1e-4;
a = [1];
b = [alpha, alpha*exp(-1j*2*pi*beta1)]/(1 - alpha);
figure;
freqz(b, a, 1e5, "whole")
%}

%{
alpha = 1;
n = 3;
a = [1];
b = (alpha*exp(-1j*2*pi*beta1)).^((0:(n -1)).')/n;
figure;
freqz(b, a, 1e5, "whole")
%}

%{
alpha = 1 - 1e-5;
r = 10*log10((1 - alpha)/abs(1 - alpha*exp(-1j*2*pi*(beta2-beta1))))
a = [1];
b = [1, -alpha*exp(-1j*2*pi*beta1)]/abs(1 - alpha*exp(-1j*2*pi*(beta2-beta1)));
figure;
freqz(b, a, 1e5, "whole")
%}


bw = beta1/2
alpha = 1 - 1e-3;
b = [1];
a = [1, -alpha*exp(1j*2*pi*beta1)];

a2 = [1, -alpha*exp(1j*2*pi*(beta1 + bw/2))];
a3 = [1, -alpha*exp(1j*2*pi*(beta1 - bw/2))];
a = conv(a, conv(a2, a3));
a = a/((1 -alpha)*(1-alpha*exp(1j*2*pi*bw/2))*(1-alpha*exp(-1j*2*pi*bw/2)));

figure;
freqz(b, a, 1e5, "whole")

%{
bw = beta1/2

[n, w, betak, ftype] = kaiserord ([beta1 - bw, beta1 - bw/2, beta1 + bw/2, beta1 + bw], [1e-1, 1, 1e-1], [1, 1e-3, 1])
b = fir1 (n, w, kaiser (n+1, betak), ftype, "noscale");

freqz (b, 1, 1e5, "whole");
%}

%{
bw = beta1/2
[n, w, betak, ftype] = kaiserord ([beta1 + bw/2, beta1 + bw], [1, 1e-1], [1e-3, 1])
b = fir1 (n, w, kaiser (n+1, betak), ftype, "noscale");

freqz (b, 1, 1e5, "whole");
%}

bw = 100*beta1;

[n, w, betak, ftype] = kaiserord ([bw/2, 1.1*bw/2], [1, 1e-1], [1e-3, 1])

b = fir1 (n, w, kaiser (n+1, betak), ftype, "noscale");
freqz (b, 1, 1e5, "whole");
