%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [symbols, offset] = lora_demodulate(freq, SF, OSF)
  symbol_length = 2^SF*OSF;

  n_sym = floor(length(freq)/symbol_length);
  symbols = zeros(n_sym, 1);

  upchirp = lora_chirp_freq(SF, OSF);
  offset = 0;
  
  for sym = 1:n_sym
    if sym == n_sym && n_sym*symbol_length + round(offset) > length(freq)
      disp("lora_demodulate: not enough samples to correct for re-alignment");
      offset = 0;
    end;
    
    symi = freq((sym-1)*symbol_length+(1:symbol_length) + round(offset));
    
    r = ifft(fft(symi).*conj(fft(upchirp)));%%circular correlation
    [maxr, i] = max(r);
    
    %%re-align
    ind_error = (i - 1) - OSF*round((i - 1)/OSF)
    offset = offset + 0.1*ind_error
   
    symbols(sym) = mod(-round((i - 1)/OSF), 2^SF);

    if(sym <= 8)
      figure;
      hold on;
      plot(symi);
      plot(lora_get_symbol_freq(symbols(sym), SF, OSF));
      legend("Symbol Samples", "Detected Symbol Samples");
      title(["Received and ideal symbol samples for n_{sy} = " int2str(symbols(sym)) ", SF = " int2str(SF)]);
      ylabel("Normalized Instantaneous Frequency");
      xlabel("Sample");
    end;
  end;
end
