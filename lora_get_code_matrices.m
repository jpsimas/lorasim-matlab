%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [G H] = lora_get_code_matrices(CR)
  switch CR
    case 1
      P = ones(4,1);%parity check
%%    otherwise
      %%      P = 1 - eye(4,CR);%%shortened hamming(6,4), hamming(7,4) or extended hamming(8, 4)
      %% codes gotten from rpp0/gr-lora
    case 2
      P = [1 1 1 0; 0 1 1 1].';
    case 3
      P = [1 1 1 0; 0 1 1 1; 1 1 0 1].';
    case 4
      P = [1 1 1 0; 0 1 1 1; 1 1 0 1; 1 0 1 1].';
  end
  
  G = [eye(4),P];
  %%G = G(size(G, 1):-1:1, :);
  H = [P;eye(CR)];
  %%H = H(:, size(H, 2):-1:1);
end
