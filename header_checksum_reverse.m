frames = de2bi([0x01803
		0x0280c
		0x0380a
		0x0480f
		0x0130a
		0x0150e
		0x01709
		0x01908
		0x02907
		0x03901
		0x04904
		0x1791a
		0xff908
		0x3c90b], 20, 'left-msb');%%note that nibbles are in reverse order

%%remove unnecessary bits


%%modulus 2 gaussian elimination
curr_pos = 1;
for col = 1:size(frames, 1)
  one_found = false;
  for row = curr_pos:size(frames,1)
    if frames(row, col) == 1
      if ~one_found
	a = frames(curr_pos, :);
	frames(curr_pos, :) = frames(row, :);
	frames(row, :) = a;
	one_found = true;
      else
	frames(row, :) = mod(frames(row, :) + frames(curr_pos, :), 2);
      end
    end
  end

  %%frames
  
  if one_found
    for row = 1:(curr_pos - 1)
      if frames(row, col) == 1
	frames(row, :) = mod(frames(row, :) + frames(curr_pos, :), 2);
      end
    end
    
    curr_pos = curr_pos + 1;
  end

  %%frames
  
  %%CHECK
  if one_found
    fail = false;
    for row = 1:size(frames, 1)
      if frames(row, col) == 1 && row ~=curr_pos-1
	disp("ALGORITHM FAILED");
	curr_pos
	col
	fail = true;
	break;
      end;
    end
    if fail
      break
    end;
  end
end


frames

triggered_bits = zeros(12, 8);

for i = 1:size(frames, 1)
  ind = find(frames(i, 1:12));
  if length(ind) == 1
    pos = ind(1)
    triggered_bits(pos, find(frames(i, 13:end))) = 1;
  end;
end

triggered_bits

for checksum_bit = 1:size(triggered_bits, 2)
  find(triggered_bits(:, checksum_bit))-1
end;
