%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function y = lora_frequency_filter(x, OSF, SF)
  nw = 2^SF;
  y = zeros(size(x, 1), 1);
  f = zeros(nw, 1);
  alpha = 0.9;
  I = (0:(length(x)-1)).';
  beta = 1/(OSF^2*2^SF);
  chirperino = (exp(1j*pi*beta*I.^2));
  x = x.*conj(chirperino);

  figure;
  specgram(x, nw);
  title("dechirped");
  
  for i = 1:nw:size(x, 1) - 1 - nw%% - k;
    xi = x(i:(i + nw - 1));
    q = fft(xi);
    f = (alpha*abs(f) + (1 - alpha)*abs(q)).*sign(q);
    y(i:(i + nw - 1)) = ifft(f);
  end;

  figure;
  specgram(y, nw)
  title("dechirped filtered");  
  
  y = y.*chirperino;
end;
