%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_frequency_track_new(x, OSF, SF)
  symbol_length = 2^SF*OSF;
  nw = 2;%%3;%%should be at least number of frequencies (i.e. number of different sfs)
  delta = OSF;
  
  freq = zeros(size(x, 1) - 1, 1);

  mu = 1e-3/OSF;
  
  w = ones(nw, 1)/nw;
  
  beta = 1/(OSF^2*2^SF);
  %%w = exp(-1j*2*pi*beta*((length(w)- 1):-1:0).');

  I = (0:(length(x)-1)).';
  chirperino = (exp(1j*pi*beta*I.^2));

  x = x.*conj(chirperino);

  figure;
  specgram(x);
  
  depth = OSF;
  
  D = zeros(1, depth);
  X = zeros(nw, depth);
  
  %%beta = 1/(OSF*2^SF);
  %%k = OSF;
  for i = 1:size(x, 1) - 1 - nw*delta%% - k;
    xi = x(i:delta:(i + delta*nw - 1));
    di = nw*x(i + delta*nw);


    D(2:end) = D(1:(end-1));
    D(:, 1) = di;
    X(:, 2:end) = X(:, 1:(end-1));
    X(:, 1) = xi;

    Y = w'*X;
    Err = D - Y;

    w += mu*((delta*eye(size(X)(2))+X'*X)/X)'*Err';

    r = roots([1; -w(end:-1:1)]);

    [emin, imax] = min(abs(abs(r) - 1));
    
    freq(i) = r(imax);
  end;
  
  figure;
  plot(20*log10(abs(fft(freq))));
  title("EBIN (dB)");

  figure;
  plot(20*log10(abs(freq)));
  title("power (dB)");
  
  %%M
  figure;
  %%plot(arg(c));
  title("BENIS");
  %%freq = freq.*exp(-1j*2*pi*beta*(0:length(freq)-1).');
  freq = -angle(freq)/(2*pi);

  
  I = (0:(size(freq, 1) - 1)).';
  freq = mod(freq + beta*I, 1);%%delay of the frequecy signal relative to the input
  
  
  freq = mod(freq + 0.5, 1) - 0.5;

  plot(freq);
end;
