%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_calc_frequecy_chirp_window(x, OSF, SF)

  pkg load signal;
  
  freq = zeros(size(x, 1), 1);
  ampl = zeros(size(x, 1), 1);
  freq2 = zeros(size(x, 1), 1);
  ampl2 = zeros(size(x, 1), 1);

  freq2_1 = zeros(size(x, 1), 1);
  freq2_2 = zeros(size(x, 1), 1);
  
  is_downchirp = zeros(size(x, 1), 1);
  
  symbolLength = 2^SF * OSF;

  %%FIND A WAY OF USING samples spaced by OSF
  
  x2 = conj(x);

  delta = 1;
  %%cutoff = symbolLength/8;%%too small to reject interference
  alpha = 0;%%-log(1/2)/cutoff^2;
  %%n_real = 2*ceil(sqrt(3*log(10)/alpha)) + 1
  n_dft = 2*symbolLength;
  DFT = zeros(delta*n_dft, 1);
  DFT2 = zeros(delta*n_dft, 1);

  figure;
  specgram(x, n_dft);


  %%chirp window
  beta = 1/(OSF^2*2^SF);
  I = (0:(n_dft-1)).' - (n_dft-1)/2;
  chirperino = (exp((1j*pi*beta - alpha)*I.^2));
  window_width = symbolLength/2;
  chirperino(1:(n_dft - window_width)/2) = 0;
  chirperino((end-(n_dft - window_width)/2 - 1):end) = 0;

  figure;
  hold on;
  plot(real(chirperino));
  plot(imag(chirperino));
  %%chirperino = exp(1j*pi/(symbolLength + 1)*I.^2);
  
  chirperino = fft(conj(chirperino));

  figure;
  hold on;
  plot(real(chirperino));
  plot(imag(chirperino));
  title("chirperino DFT");
  
  exponents = exp(1j*2*pi*(0:(size(DFT, 1) - 1)).'/(size(DFT, 1)));
  exponentsN = exponents.^n_dft;
  
  for i = 1:min(n_dft, size(x, 1))
    DFT = exponents.*DFT + x(i);
    DFT_windowed = cconv(chirperino, DFT, n_dft);
    [ampl(i), freq(i)] = max(abs(DFT_windowed));
    
    DFT2 = exponents.*DFT2 + x2(i);
    DFT2_windowed = cconv(chirperino, DFT2, n_dft);
    [ampl2(i), freq2(i)] = max(abs(DFT2_windowed));

    freq2_1(i) = DFT_windowed(freq(i))*exp(1j*2*pi*freq(i)*(n_dft - 1)/2/n_dft);
    freq2_2(i) = DFT2_windowed(freq2(i))*exp(1j*2*pi*freq2(i)*(n_dft - 1)/2/n_dft);
  end;
  
  for i = (n_dft + 1):size(x, 1)
    DFT = exponents.*DFT + x(i) - x(i - n_dft).*exponentsN;
    DFT_windowed = cconv(chirperino, DFT, n_dft);
    [ampl(i), freq(i)] = max(abs(DFT_windowed));
    
    DFT2 = exponents.*DFT2 + x2(i) - x2(i - n_dft).*exponentsN;
    DFT2_windowed = cconv(chirperino, DFT2, n_dft);
    [ampl2(i), freq2(i)] = max(abs(DFT2_windowed));

    freq2_1(i) = DFT_windowed(freq(i))*exp(1j*2*pi*freq(i)*(n_dft - 1)/2/n_dft);
    freq2_2(i) = DFT2_windowed(freq2(i))*exp(1j*2*pi*freq2(i)*(n_dft - 1)/2/n_dft);
  end;
  
  freq = (freq - 1);
  freq = freq/(size(DFT, 1));

  freq2 = (freq2 - 1);
  freq2 = freq2/(size(DFT2, 1));
  
  freq2 = mod(-freq2, 1);

 freq = mod(freq + 0.5, 1) - 0.5;
 freq2 = mod(freq2 + 0.5, 1) - 0.5;
  
 figure;
 hold on;
 plot(freq);
 plot(freq2);
 
 %%freq = freq + 1j*freq2; 
 is_downchirp = (ampl2 > ampl);
 freq = (~is_downchirp).*freq + (is_downchirp).*freq2;

 figure;
 hold on;
 plot(freq);
 plot(is_downchirp);
 
  figure;
  hold on;
  plot(ampl/size(DFT, 1));
  plot(ampl2/size(DFT, 1));
  title("amplitudes");

  %%figure;
  %%freq = (~is_downchirp).*freq2_1 + (is_downchirp).*conj(freq2_2);
  %%specgram(freq);
end;
