%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_align_demod_lazy(freq, SF, OSF, syncword_number)
  clear x;
  
  figure;
  plot(freq);
  
  symbol_length = 2^SF*OSF;

  upchirp = lora_chirp_freq(SF, OSF);
  %%minimal preamble (indicates start of frame)
  %%TODO: implement syncword as a parameter

  %%sync_word = [upchirp; upchirp];
  %%sync_word = [upchirp(1 + mod(24*OSF + (0:symbol_length-1), symbol_length)); upchirp(1 + mod(32*OSF + (0:symbol_length-1), symbol_length))];

  %%syncword number: higher nibble represents first symbol and lower, the second one
  
  syncword_number = mod(round(syncword_number), 0x100);%truncate to a byte
  sync_sym1 = 2^(SF - 5)*floor(syncword_number/0x10);
  sync_sym2 = 2^(SF - 5)*mod(syncword_number, 0x10);

  n_upchirps = 5;
  preamble = [zeros(5, 1); sync_sym1; sync_sym2; 0; 0]/symbol_length;

  preamble_length = symbol_length*length(preamble);
  
  ind = 1;
  found = false;
  
  M_max = 0;
  
  i = 1;
  while i <= (length(freq) - preamble_length + 1)
    freqi = freq(i + ((0:symbol_length:(preamble_length - 1))));
    M = 1 - norm(mod(freqi - preamble, 1));

    if ~found
      if M >= 1 - 1e-6
	ind = ind - (i - 1);
	freq = freq((i):end);
	i = 1;
	found = true
      end;
    else
      if M < 0.7
	break;
      end;
    end;

    if M >= M_max
      M_max = M;
      ind = i;
    end;
    i = i + 1;
  end
  
  if ~found
    disp("ALIGN ERROR: PREAMBLE NOT FOUND");
  end;
  M_max
  
  figure;
  hold on;
  plot(freq(ind:symbol_length:(ind + preamble_length - 1)));
  
  plot(preamble);

  freq = freq((ind + preamble_length + symbol_length/4 - OSF - symbol_length/2):end);

  figure;
  plot(freq);
  
  freq = round(mod(symbol_length*freq((symbol_length/2):symbol_length:end), 2^SF));

end
 
