function [cl cl_found] = get_coset_leaders_lut(H)
  size_H = size(H);
  CR = size_H(2);

  code_length = size_H(1);
  
  cl = zeros(2^CR,code_length);
  cl_found = zeros(2^CR-1,1);
  for i = 1:code_length
    syn = H(i,:) * (2.^(CR-1:-1:0).');
    if ~cl_found(syn)
      cl(syn,i)=1;
      cl_found(syn) = 1;
    else
      cl_found(syn) = 2;
    end
  end
  if any(cl_found==0)
    for i1 = 1:code_length-1
      for i2 = i1+1:code_length
	syn = mod(H(i1,:)+H(i2,:),2) * (2.^(CR-1:-1:0).');
	if ~cl_found(syn)
	  cl(syn,[i1,i2])=1;
	  cl_found(syn) = 1;
	else
	  cl_found(syn) = 2;
	end
      end
    end
  end
end
   
