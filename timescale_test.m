close all;
clear all;

I = (0:99).';
f = 0.02;
%%x = sin(2*pi*f*I);
x = I;

figure;
plot(x);

x_f = fft(x);

figure;
plot(abs(x_f));

freq_ratio = 1/2;
new_length = floor(length(x)/freq_ratio);

x_shift = zeros(new_length, 1);
I = (0:(length(x_shift) - 1)).';
size(x_shift)

for k = 0:(min(length(x_shift), length(x_f)) - 1)
  x_shift = x_shift + x_f(k + 1)*exp(1j*2*pi*mod(freq_ratio*k, length(x_f))/length(x_f)*I);
end;

x_shift = real(x_shift)/length(x_shift);

size(x_shift)

figure;
hold on;
plot(x);
plot(x_shift);
