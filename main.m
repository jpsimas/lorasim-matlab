%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

close all;
clear all;

SF = 7;
CR = 4;

OSF = 8;

n_bits = 7*8;
%% SNR = 10.^(linspace(-30, -25, 10)/10);
SNR = 1e3;
SIR = inf;
SFinterf = SF + 1;
%%SNR = 10;

syncword_number = 0x00;
frequency_offset_relative = 0.25;

implicit_mode = false;
low_data_rate = false;
payload_crc_present = true;

for i = 1:length(SNR)
  BER(i) = lora_sim(n_bits, CR, OSF, SF, syncword_number, frequency_offset_relative, implicit_mode, low_data_rate, payload_crc_present, SNR(i), SIR(i), SFinterf);
end

BER

figure;
hold on;
plot(10*log10(SNR), 10*log10(BER));

xlabel("SNR (dB)");
ylabel("10 log_{10}(Error Rate)");

legend("BER");
