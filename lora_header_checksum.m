%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function checksum = lora_header_checksum(header)
  %%NOTE: incomplete, behaviour due to bits 0 1 2 4 of header missing
  checksum = zeros(5, 1);
%%  checksum(1) = mod(sum(header([3] + 1)), 2);
%%  checksum(2) = mod(sum(header([5 6 11] + 1)), 2);
%%  checksum(3) = mod(sum(header([7 8 10] + 1)), 2);
%%  checksum(4) = mod(sum(header([5 7 9 10 11] + 1)), 2);
  %%  checksum(5) = mod(sum(header([3 6 8 9 10 11] + 1)), 2);

  checksum(1) = mod(sum(header([0 5 8 9 10 11] + 1)), 2);
  checksum(2) = mod(sum(header([1 4 6 8 9 10] + 1)), 2);
  checksum(3) = mod(sum(header([2 4 7 9 11] + 1)), 2);
  checksum(4) = mod(sum(header([3 5 6 7 8] + 1)), 2);
  
  checksum(5) = mod(sum(header([0 1 2 3] + 1)), 2);
  checksum(6) = 0;
  checksum(7) = 0;
  checksum(8) = 0;
end
