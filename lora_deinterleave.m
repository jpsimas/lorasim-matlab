%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function c_est = lora_deinterleave(signal_est, CR)
  [n_syms SF] = size(signal_est);

  n_codedbits = n_syms*SF;
  
  code_rank = 4;
  code_length = code_rank + CR;
  
  n_bits = n_codedbits*code_rank/code_length;
  interleaver_size = code_length*SF;
  n_blocks = n_codedbits/interleaver_size;
  n_codewords = n_bits/code_rank;

  c_est = zeros(n_codewords, code_length);
  for i = 0:(n_blocks - 1)
    for k = 0:(code_length - 1)
      for m = 0:(SF - 1)
	c_est(i*SF + mod(m + k, SF) + 1, k + 1) = signal_est(i*code_length + k + 1, m + 1);
      end
    end
  end 
end
