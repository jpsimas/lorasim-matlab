%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function y = lora_whitening_transform(S_num, SF, low_data_rate, explicit)
  %% get whitening sequence
  seq = lora_get_whitening_sequence(SF, low_data_rate, explicit);
  
  seq_pos = 0;

  y = bitxor(S_num, seq(mod(0:(length(S_num) - 1), length(seq)) + 1));
  
end

  
