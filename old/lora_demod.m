%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function symbols = lora_demod(r, SF, OSF)
  sym_length = 2^SF*OSF;

  n_sym = length(r)/sym_length;
  symbols = zeros(n_sym, 1);

  upchirp = lora_chirp(1, SF, 0, OSF);
  r = r.*conj(upchirp(mod(0:(length(r)-1), length(upchirp))+1));
  
  for sym = 1:n_sym
    z = fft(r((sym-1)*sym_length+(1:OSF:sym_length)));
    [max1, pos] = max(abs(z));
    symbols(sym, :) = pos - 1;
  end
end
