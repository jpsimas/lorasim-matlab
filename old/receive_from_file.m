%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

pkg load signal;
close all;
clear all;

SF = 7;
CR = 4;

fs = 10;
bw = 0.125/fs;
fc = 2.1/fs;
%%OSF = 80;
OSF = round(1/bw)

file = fopen("./sample_data/gr-lora-samples/00_crcoff_cr4_sf7.cfile");
n_samples = inf;
data = fread(file, n_samples, 'int16 => double', 0);
%%%data = data((3.5e7:4.5e7));
data = data(3.5e7 + (1.7e6:3.1e6));
fclose(file);

data(1:10)

figure;%%('visible', 'off');
plot(data);
print -dpng data.png;

data = data(1:2:end);
data = data(1:2:end-1) + 1j*data(2:2:end);
data(1:10)

%%data = ifft(real(fft(data)));
%%data = data - mean(data);
%%figure;
%%plot(real(data))
%%title("real part");

figure;
specgram(data, 1024, 1);
title("spectrogram");

t = (0:(length(data) - 1)).';
data = data.*exp(-1j*2*pi*fc*t);

figure;
specgram(data,1024, 1);
title("spectrogram downconverted");

figure;
plot(real(data(1:100)));
title("real part downconverted");


%%low-pass
bw_filter = 1.5*bw;
[n, w, beta, ftype] = kaiserord ([bw_filter 1.1*bw_filter], [1, 0], [0.05, 0.05]);
b = fir1 (n, w, kaiser (n+1, beta), ftype, "noscale");

figure;
freqz (b, 1, []);

size(b)
data = filter(b, 1, data);

figure;
specgram(data,1024, 1);
title("specgram lp");

%%get first frame
non_zero_i = find(abs(data) > 2500);
data = data(non_zero_i(1):end).';

clear non_zero_i;

figure;
%%data = data((1e5 + 24765):3e5);
plot(real(data));

figure;
specgram(data, 1024, 1);
title("specgram data");

%%try to generate same signal
data_sim = lora_tx(zeros(8, 1), CR, OSF, SF, false, false, false);
figure;
t = (0:(length(data_sim) - 1)).';
specgram(data_sim.*exp(1j*2*pi*fc*t),1024, 1);
title("spectrogram of simulated data");

bits = lora_rx(data.', OSF, SF, false, false, false)


