%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function x_al = lora_align(x, SF, OSF)
  sym_length = 2^SF*OSF;

  upchirp = lora_chirp(1, SF, 0, OSF);
  %%minimal preamble (indicates start of frame)
  %%NOTE: ASSUMES uc uc syncword (not true for lorawan
  %%TODO: implement syncword as a parameter
  %%minimal_preamble = [upchirp; upchirp; conj(upchirp); conj(upchirp); conj(upchirp((1:(sym_length/4))))];
  
  %%IGNORING syncword tempoarily
  minimal_preamble = [conj(upchirp) conj(upchirp) [conj(upchirp((1:(sym_length/4)))); zeros(3*sym_length/4,1)]];
  
  preamble_length = size(minimal_preamble, 1)*size(minimal_preamble, 2);
  preamble_norm = norm(minimal_preamble);

  %%figure;
  %%specgram(minimal_preamble, 8*OSF, 1);
  %%title("PREAMBLE");

  ind = 1;
  
  %%  M_min = (((2 + 1/4)/4) + 2/4)/2;
  M_max = 0;
  M_vec = zeros((length(x) - preamble_length + 1), 1);
  for i = 1:(length(x) - preamble_length + 1)
    %%assumes signal has unit power
    
    M = 0;
    for k = 1:size(minimal_preamble, 2)
      xi = x((i + (k-1)*sym_length):(i + k*sym_length - 1));
      M = M + abs(dot(xi, minimal_preamble(:, k))/(preamble_norm*norm(xi)))^2;
    end

    if M >= 0.7
      break;
    end;
      
    M_vec(i) = M;
    if M >= M_max
      M_max = M;
      ind = i;
    end;
  end;

  M_max
  figure;
  plot(M_vec);

  %% align, remove preamble and return
  x_al = x((ind + (2 + 1/4)*sym_length):end);
end
