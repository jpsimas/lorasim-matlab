%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function s = lora_chirp(mu, SF, phi0, OSF)
  N = 2^SF*OSF;
  I = (((0:N-1) - (N - 1)/2)).';
  phi = 2*pi*mu*1/OSF*(1/(N - 1)*I.^2/2 + (N - 1)/8);
  
  s = exp(1i*(phi0+phi));
end
