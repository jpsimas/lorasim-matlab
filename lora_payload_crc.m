%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function bits_crc = lora_payload_crc(bits)
  pol_inds = find(de2bi(0x1021));
  pol = zeros(17, 1);
  pol(end) = 1;
  pol(pol_inds) = 1;
  bits_crc = modulo2_polynomial_division(lora_invert_endianness(bits), pol);
  bits_crc = lora_invert_endianness(bits_crc);
end;
