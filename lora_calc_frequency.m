%% lorasim-matlab
%% Copyright (C) 2020 Jo�o Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_calc_frequency(x, OSF, SF)
  freq = zeros(size(x, 1), 1);
  ampl = zeros(size(x, 1), 1);
  freq2 = zeros(size(x, 1), 1);
  ampl2 = zeros(size(x, 1), 1);

  freq3 = zeros(size(x, 1), 1);
  freq4 = zeros(size(x, 1), 1);
  lambda = zeros(size(x, 1), 1);
  a = 0;
  k_a = 3000;

  is_downchirp = zeros(size(x, 1), 1);
  
  symbolLength = 2^SF * OSF;


  %%FIND A WAY OF USING samples spaced by OSF
  
  %%multiply signal by downchirp before calculating frequency
  beta = 1/(OSF^2*2^SF);
  I = (0:(length(x)-1)).';
  chirperino = (exp(1j*pi*beta*I.^2));
  
  figure;
  specgram(chirperino);
  
  %%x2 = x.*chirperino;
  x2 = conj(x).*conj(chirperino);
  
  x = x.*conj(chirperino);
  %%x = (x + conj(x)).*(conj(chirperino));
  %%x = x.*(conj(chirperino) + chirperino);
  %%x = conj(x).*conj(chirperino);%%32
  %%x = x.*chirperino;%%96 = -32 (mod 128)

  delta = 4;
  n_dft = symbolLength/2;
  DFT = zeros(delta*n_dft, 1);
  DFT2 = zeros(delta*n_dft, 1);

  figure;
  specgram(x, n_dft);
  
  alpha = 0;
  
  exponents = (1 - alpha)*exp(1j*2*pi*(0:(size(DFT, 1) - 1)).'/(size(DFT, 1)));
  exponentsN = exponents.^n_dft;
  %%freqs = (0:size(DFT, 1) - 1).'/size(DFT, 1);
  
  is_downchirp_last = false;
  
  for i = 1:min(n_dft, size(x, 1))
    DFT = exponents.*DFT + x(i);
    [ampl(i), freq(i)] = max(abs(DFT));
    
    DFT2 = exponents.*DFT2 + x2(i);
    [ampl2(i), freq2(i)] = max(abs(DFT2));

    freq3(i) = DFT(freq(i));
    freq4(i) = DFT2(freq2(i));

    %%a = 0.1*a + 0.9*(ampl2(i)^2 - ampl(i)^2)/(size(DFT, 1)^2);
    %%lambda(i) = 1/(1 + exp(-k_a*a));
    %{
    if(~is_downchirp_last && (ampl2(i) - ampl(i))/size(DFT, 1) > -1e-1)
      is_downchirp_last = true;
    else
      if(is_downchirp_last && (ampl(i) - ampl2(i))/size(DFT, 1) > -1e-1)
	is_downchirp_last = false;
      end;
    end;

    is_downchirp(i) = is_downchirp_last;
    %}
    %%is_downchirp(i) = (ampl2(i) > ampl(i));
  end;
  
  for i = (n_dft + 1):size(x, 1)
    DFT = exponents.*DFT + x(i) - x(i - n_dft).*exponentsN;
    [ampl(i), freq(i)] = max(abs(DFT));
    
    DFT2 = exponents.*DFT2 + x2(i) - x2(i - n_dft).*exponentsN;
    [ampl2(i), freq2(i)] = max(abs(DFT2));

    freq3(i) = DFT(freq(i));
%%    if(abs(freq3(i)) < 0.8*n_dft)
%%      freq3(i) = 0;
%%    end;
    
    freq4(i) = DFT2(freq2(i));

%%    if(abs(freq4(i)) < 0.8*n_dft)
%%      freq4(i) = 0;
%%    end;
    
    %%a = 0.1*a + 0.9*(ampl2(i)^2 - ampl(i)^2)/(size(DFT, 1)^2);
    %%lambda(i) = 1/(1 + exp(-k_a*a));
    %{
    if(~is_downchirp_last && (ampl2(i) - ampl(i))/size(DFT, 1) > -1e-1)
      is_downchirp_last = true;
    else
      if(is_downchirp_last && (ampl(i) - ampl2(i))/size(DFT, 1) > -1e-1)
	is_downchirp_last = false;
      end;
    end;
    
    is_downchirp(i) = is_downchirp_last;
    %}

    %is_downchirp(i) = (ampl2(i) > ampl(i));
  end;

  is_downchirp = (is_downchirp > 0.5);
  
  freq = (freq - 1);
  freq = freq/(size(DFT, 1));

  freq2 = (freq2 - 1);
  freq2 = freq2/(size(DFT2, 1));

  figure;
  specgram(freq3);
  title("freq3");
  
  figure;
  freq3 = freq3.*chirperino.*exp(-1j*2*pi*beta*(n_dft - 1)/2.*I);
  freq4 = freq4.*chirperino.*exp(-1j*2*pi*beta*(n_dft - 1)/2.*I);
  is_downchirp = (abs(freq4) > abs(freq3));
  freq3 = (~is_downchirp).*freq3 + (is_downchirp).*conj(freq4);
  specgram(freq3);
  title("freq3");

  figure;
  hold on;
  plot(abs(freq3)/n_dft);
  plot(is_downchirp);
  title("abs freq3");
  
%  freq3 = (freq3 - 1);
%  freq3 = freq3/(size(DFT2, 1));
  
  %{
  alpha = 1e-3;
  freq = filter([1], [1; -alpha]/(1 - alpha), freq);

  freq2 = filter([1], [1; -alpha]/(1 - alpha), freq2);  
  %}

%%  freq = mod(freq + beta*(I  - 1/(2*beta*OSF)) + 0.5, 1) - 0.5;

%%  freq = mod(freq*OSF + 0.5, 1) - 0.5;
  
 freq = mod(freq + beta*(I - (n_dft - 1)/2), 1);%%delay of the frequecy signal relative to the input

 freq2 = mod(freq2 + beta*(I - (n_dft - 1)/2), 1);

 freq2 = mod(-freq2, 1);
 
 figure;
 hold on;
 plot(mod(freq + 0.5, 1) - 0.5);
 plot(mod(freq2 + 0.5, 1) - 0.5);

 freq = mod(freq + 0.5, 1) - 0.5;
 freq2 = mod(freq2 + 0.5, 1) - 0.5;
 
 %%freq = freq + 1j*freq2;
 freq = (~is_downchirp).*freq + is_downchirp.*freq2;

 %{
 figure;
 freq3 = mod(freq3 + beta*(I - (size(DFT, 1) - 1)/2), 1);
 freq3 = mod(freq3 + 0.5, 1) - 0.5;
 plot(freq3);
 title("freq3");
 %}

 %{
 freq3 =  mod((1 - lambda).*freq + lambda.*freq2, 1);
 
 freq = mod((~is_downchirp).*freq + (is_downchirp).*freq2, 1);
 
  size(freq)
  size(is_downchirp)

  freq = mod(freq + 0.5, 1) - 0.5;

  freq3 = mod(freq3 + 0.5, 1) - 0.5;
  
  figure;
  hold on;
  plot(freq);
  plot(is_downchirp);
  figure;
  hold on;
  plot(freq3);
  plot(lambda);
  title("freq3");
%}
  figure;
  hold on;
  plot(ampl/size(DFT, 1));
  plot(ampl2/size(DFT, 1));
  title("amplitudes");

%%  plot(2^SF*freq2);
%%  plot(2^SF*is_downchirp);
%%  plot(2^SF*mod(OSF*freq, 1));
  %%  plot(2^SF*mod(OSF*freq2, 1));


  %%freq = freq3;
end;
