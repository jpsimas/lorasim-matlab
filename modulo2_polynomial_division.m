%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [remainder, quotient] = modulo2_polynomial_division(dividend, divisor)
  if sum(dividend) == 0
    remainder = 0;
    quotient = 0;
    return;
  end;
  
  f1 = find(dividend);
  f2 = find(divisor);
  degree1 = f1(end);
  degree2 = f2(end);

  dividend = dividend(1:degree1);
  divisor = divisor(1:degree2);
  
  offset = degree1 - degree2;%%degree difference
  if offset < 0
    remainder = [dividend; zeros(length(divisor) - length(dividend) - 1, 1)];
    quotient = [];
    return;
  end;

  quotient = zeros(offset + 1, 1);
  
  while offset >= 0
    if dividend(length(divisor) + offset) == 1
      dividend((1:length(divisor)) + offset) = mod(dividend((1:length(divisor)) + offset) + divisor, 2);
      quotient(offset + 1) = 1;
    end;
    offset = offset - 1;
  end;

  remainder = dividend(1:(length(divisor)-1));
end;
