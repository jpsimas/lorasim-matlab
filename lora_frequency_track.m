%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function freq = lora_frequency_track(x, OSF, SF)
  freq = zeros(size(x, 1) - 1, 1);
  %%  mu = 1/OSF;
  %%mu = 0.1;%%0.1;
  %%mu = max(0.1, min(1, 2/OSF));
  mu = 2/OSF;

  w = 1;

  %%  k = OSF;

  beta = 1/(OSF^2*2^SF);
  %%beta = 1/(OSF*2^SF);
  for i = 1:size(x, 1) - 1%% - k;
    %%    xi = x(i:(i + k - 1));
    %%    di = x((i + 1):(i + k));
    %%w = w + mu*(di - w'*xi)'*xi/dot(xi, xi);

    %%w = w + mu*conj(x(i + 1) - conj(w)*x(i))*x(i)/(x(i)*conj(x(i)));

    if x(i) != 0
      w = w*exp(-1j*2*pi*beta);
      %%w = (1 - mu)*w + mu*conj(x(i + 1)/x(i));
      %%w = w + mu*(x(i + 1) - w'*x(i))'*x(i);
      %%w = (1 - mu)*w + mu*conj(sign(x(i + 1)/x(i)))^OSF;
      w = (1 - mu)*w + mu*conj(sign(x(i + 1)/x(i)));
    end;

    freq(i) = w;

  end;

  freq = -angle(freq)/(2*pi);
end;
