%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function BER = lora_sim(n_bits, CR, OSF, SF, syncword_number, frequency_offset_relative, implicit_mode, low_data_rate, payload_crc_present, SNR, SIR, SFinterf)
  
  sigma_n = 1/sqrt(SNR);%%signal has unit power

  %% random source
  %%bits = randi([0 1], n_bits, 1);
  bits = zeros(n_bits, 1);
  
%%  implicit_mode = false;
%%  low_data_rate = false;
%%  payload_crc_present = false;
  
  r = lora_tx(bits, CR, OSF, SF, syncword_number, frequency_offset_relative, implicit_mode, low_data_rate, payload_crc_present);
  if(SIR ~= inf)
    r_interf = lora_tx(zeros(8, 1), CR, OSF, SFinterf, syncword_number, frequency_offset_relative, implicit_mode, low_data_rate, payload_crc_present);
    if length(r) < length(r_interf)
      r = [r; zeros(length(r_interf) - length(r), 1)];
    else
      r_interf = [r_interf; zeros(length(r) - length(r_interf), 1)];
    end;
    r = r + (1/SIR)*r_interf;
  end;
  r = r + sigma_n*randn(size(r));

  t = (0:(length(r) - 1)).';
  
  specgram(r.*exp(1j*2*pi*(1/OSF/2)*t), 8*OSF, 1);
  title("Received Signal Spectrogram");

  %%low-pass
  %%bw = 1/OSF;
  %%bw_filter = bw;
  %%[n, w, beta, ftype] = kaiserord ([bw_filter 2*bw_filter], [1, 0], [0.05, 0.05]);
  %%b = fir1 (n, w, kaiser (n+1, beta), ftype, "noscale");

  %%r = [zeros(n, 1); r; zeros(n, 1)];
  %%r = filter(b, 1, r);

  r = [r; zeros(OSF*2^SF*OSF, 1)];%%add some zeros at the end
  
  if implicit_mode
    payload_length = ceil(length(bits)/8);
    bits_est = lora_rx(r, OSF, SF, syncword_number, implicit_mode, low_data_rate, false, payload_length, CR, payload_crc_present);
  else
    bits_est = lora_rx(r, OSF, SF, syncword_number, implicit_mode, low_data_rate, false);
  end;
  
  bit_errors = sum(bits(:) ~= bits_est(:));
  BER = bit_errors/length(bits);

end
