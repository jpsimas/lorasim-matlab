%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function s = lora_tx(bits, CR, OSF, SF, syncword_number, frequency_offset_relative, implicit_mode, low_data_rate, payload_crc_present)
  pkg load communications;

  if payload_crc_present
    payload_crc_length = 2;
  else
    payload_crc_length = 0;
  end;

  n_bits = length(bits);
  n_bits = 8*ceil(n_bits/8);%%increase so it is a whole number of bytes

  bits = [bits; zeros(n_bits - length(bits), 1)];

%%%CALCULATE CRC
  if payload_crc_present
    bits_crc = lora_invert_endianness(lora_payload_crc(bits));
    disp("BITS/BITSCRC");
    bits.'
    bits_crc.'
  end;
  
%%%RANDOMIZE DATA
  bits = lora_randomize(bits);

  if ~implicit_mode
%%%GENERATE HEADER
    payload_length = ceil(n_bits/8)
    
    %%bits_header = [de2bi(payload_length, 8, 'left-msb') de2bi(CR, 3, 'left-msb') payload_crc_present].';
    bits_header = [de2bi(payload_length, 8) payload_crc_present de2bi(CR, 3)].';
    bits_header(1:8) = bits_header([5:8 1:4]);%%swap nibbles of the payload length

    %%    bits_header = [bits_header; zeros(3, 1); lora_header_checksum(bits_header)]
    bits_header = [bits_header; lora_header_checksum(bits_header)]
    bits_header(13:20) = bits_header([17:20 13:16]);%%swap nibbles of the crc

    %%invert bit order of the nibbles
%%    bits_header = lora_invert_nibbles(bits_header);
    
%%%ENCODE HEADER (CR = 4 always)
    %%generate code matrices
    [G H] = lora_get_code_matrices(4);

    %% coset leaders LUT
    [cl cl_found] = get_coset_leaders_lut(H);
    
    %% Hamming encoding
    C_header = lora_encode(bits_header, G);
  end;
  
%%%ENCODE PAYLOAD  
  %%generate code matrices
  [G H] = lora_get_code_matrices(CR);

  %% coset leaders LUT
  [cl cl_found] = get_coset_leaders_lut(H);


  %%CALCULATE PAYLOAD CRC - to be implemented (zeros for now)
  if payload_crc_present
    bits = [bits; bits_crc];
  end

  if low_data_rate
    SF_interleaver = SF - 2;
  else
    SF_interleaver = SF;
  end;
  
  %%add zeros to bit string so it has a length multiple of 4SF
  %%(excluding payload bits that get interleaved with the header
  if implicit_mode
    n_bits_new = 4*SF_interleaver*ceil(length(bits)/(4*SF_interleaver));
  else
    n_bits_new = 4*SF_interleaver*ceil((length(bits) - 4*(SF - 7))/(4*SF_interleaver)) + 4*(SF - 7);
  end;
  
  bits = [bits; zeros(n_bits_new - length(bits), 1)];
  
  %% Hamming encoding
  C = lora_encode(bits, G);

%%%INTERLEAVING
  if ~implicit_mode
    %% interleave header + sf - 7 codewords
    n_payload_words_in_header = (SF - 7);
    C_payload_in_header = zeros(n_payload_words_in_header, 8);
    C_payload_in_header(:, 1:(CR + 4)) = C(1:n_payload_words_in_header, :);

    C = C((n_payload_words_in_header + 1):end, :);
    
    C_ldr = [C_header; C_payload_in_header];
    S_header = lora_interleave(C_ldr, SF - 2);
  end;
  
  S = lora_interleave(C, SF_interleaver);
%%%CONCATENATE HEADER AND PLAYLOAD
  
  symbols = gray_decode(S);
  if low_data_rate
    symbols = 4*symbols;
  end;

  if ~implicit_mode
    symbols_header = gray_decode(S_header);
    symbols = [4*symbols_header; symbols]
  end;
  
  s = lora_modulate(symbols, OSF, SF, syncword_number, frequency_offset_relative, 0);
  s = [s; zeros(2^SF*OSF - 1, 1)];%%add some zeros in the end for realignment at rx
end
