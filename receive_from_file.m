%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.


function bits = receive_from_file(filename, SF, syncword_num, f_sampling, bw, f_carrier, n_samples_to_skip, n_samples_to_read)
  pkg load signal;

  %% normalize frequencies
  bw = bw/f_sampling;
  f_carrier = f_carrier/f_sampling;
  
  OSF = round(1/bw);

  file = fopen(filename);

  word_size = 4; %%size of int16 in bytes

  disp("CHECKPOINT 1");

  fseek(file, SEEK_CUR() + word_size*n_samples_to_skip);

  data = fread(file, n_samples_to_read, 'single');
  
  fclose(file);

%  figure;
%  plot(data);
%  print -dpng data.png
%  close
%  return

  data = data(1:2:end) + 1j*data(2:2:end);
  
  size(data)
  
  disp("CHECKPOINT 2");
  
  t = single(0:(length(data) - 1)).';

  size(t);
  
  disp("CHECKPOINT 3");

  t = (-1j*2*pi*f_carrier)*t;

  t = exp(t);
  
  disp("CHECKPOINT 4");
  
  data = data.*t;

  clear t;
  
  disp("CHECKPOINT 5");
  
  %%low-pass
  %% bw_filter = 2*bw;
  %% [n, w, beta, ftype] = kaiserord ([bw_filter 2*bw_filter], [1, 0], [0.05, 0.05]);
  %% b = fir1 (n, w, kaiser (n+1, beta), ftype, "noscale");

  %% data = filter(b, 1, data);

  disp("CHECKPOINT 6");

  bits = lora_rx(data, OSF, SF, syncword_num, false, false, false);
end
