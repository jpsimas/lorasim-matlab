%%clear all;
%%close all;

function w = lfsr_massey(s)
  pkg load communications;

  %%s = lora_get_whitening_sequence(6, false, false);

%%  s = de2bi(s).';
%%  size_s = size(s);

%%  s = s(size_s(1):-1:1,:);

%%  s = s(1:(size_s(1)*size_s(2)));
%%  s = s.';

 %% s = [1 0 1 0 0 1 1 1 0].';

  %%Berlekamp–Massey algorithm
  N = length(s) - 1;

  b = zeros(N, 1);
  b(end) = 1;
  c = zeros(N, 1);
  c(end) = 1;

  L = 0;
  m = -1;

  for n = 0:(N - 1)
    d = mod(dot(c((N - L + 1):N), s((n - L + 1):n)) + s(n + 1), 2);
    if d
      t = c;
      for k = 0:(N - n + m - 1)
	c(N - (n - m + k)) = mod(c(N - (n - m + k)) + b(N - k), 2);
      end;
      if L <= n/2
	L = n + 1 - L;
	m = n;
	b = t;
      end;
    end;
  end;

  w = c((N - L + 1):N);

  %%test
%%  for i = 1:(length(s) - L);
%%    test = mod(s(i + L) + dot(w, s(i:(i + L - 1))), 2);
%%    if(test)
%%      disp("FAIL");
%%      i
%%      break;
%%    end;
%%  end;
end
