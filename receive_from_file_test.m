close all;
clear all;

%% check if data files are present and if not download them
if(~exist('sample_data', 'dir'))
  disp("Files not found. Downloading them from http://research.edm.uhasselt.be/~probyns/lora/gr-lora-samples.zip");
  mkdir sample_data;
  cd sample_data;
  system('wget http://research.edm.uhasselt.be/~probyns/lora/gr-lora-samples.zip');
  system('unzip gr-lora-samples.zip');
  cd ..;
end;
%{
n_samples_to_skip = 1.82e7;
n_samples_to_read = 1e7;

disp("TEST 0: zeros, 1 byte, CRC OFF, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/00_crcoff_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;
%}
%{
disp("TEST 1: zeros, 2 bytes, CRC OFF, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/0000_crcoff_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(2*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 2: zeros, 3 bytes, CRC OFF, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/000000_crcoff_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(3*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 3: zeros 4 bytes, CRC OFF, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/00000000_crcoff_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(4*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 4: zeros, 1 byte, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/00_crcon_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 5: zeros, 2 bytes, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/0000_crcon_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(2*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 6: zeros, 3 bytes, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/000000_crcon_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(3*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 7: zeros, 4 bytes, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/00000000_crcon_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(4*8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 8: zeros, 1 byte, CRC ON, CR = 1, SF = 7");
bits = receive_from_file("./sample_data/00_crcon_cr1_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 9: zeros, 1 byte, CRC ON, CR = 2, SF = 7");
bits = receive_from_file("./sample_data/00_crcon_cr2_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

disp("TEST 10: zeros, 1 byte, CRC ON, CR = 3, SF = 7");
bits = receive_from_file("./sample_data/00_crcon_cr3_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;

n_samples_to_skip = 1.75e7;
n_samples_to_read = 1.5e7;

disp("TEST 11: ones, 1 byte, CRC OFF, CR = 3, SF = 7");
bits = receive_from_file("./sample_data/FF_crcoff_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == ones(8, 1)
  disp("PASS");
else
  disp("FAIL");
  return;
end;



n_samples_to_skip = 1.5e7 + 1.7e7;
n_samples_to_read = 1.5e7;

disp("TEST 12: zeros, 255 bytes, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/00_times_255_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read);
if bits == zeros(255*8, 1)
  disp("PASS");
else
  c = 0;
  while (bits(c + 1) == 0)
    c = c + 1;
  end;

  c
  disp("FAIL");
  return;
end;


n_samples_to_skip = 1.6e7;
n_samples_to_read = 1e7;

disp("TEST 13: counting, 23 bytes, CRC ON, CR = 4, SF = 7");
bits = receive_from_file("./sample_data/counting_cr4_sf7.cfile", 7, 0x68, 10, 0.125, 2.1, n_samples_to_skip, n_samples_to_read)
expected = de2bi([0:9 0x10:0x19 0x20:0x22], 8).';
expected = expected(1:(23*8)).';
if bits == expected
  disp("PASS");
else
  disp("FAIL");
  return;
end;
%}
%{
disp("TEST 13: madness, SF = 11");
n_samples_to_skip = 9.4e5;
n_samples_to_read = inf;
%%bits = receive_from_file("./sample_data/data_balloon2_interp_filt2.raw", 11, 0x00, 0.384, 0.125, 0, n_samples_to_skip, n_samples_to_read)
bits = receive_from_file("./sample_data/data_balloon2_interp_filt2_resampl.raw", 11, 0x00, 0.375, 0.125, 0, n_samples_to_skip, n_samples_to_read)
%}
%{
disp("TEST 14: idk, SF = 7");
n_samples_to_skip = 2*1.54e5;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 15: idk, SF = 7");
n_samples_to_skip = 2*1.8e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 16: idk, SF = 7");
n_samples_to_skip = 2*2.12e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 17: idk, SF = 7");
n_samples_to_skip = 2*2.41e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 18: idk, SF = 7");
n_samples_to_skip = 2*2.8e5;
n_samples_to_read = 5.1e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 19: idk, SF = 7");
n_samples_to_skip = 2*3.3e5;
n_samples_to_read = 2*2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 20: idk, SF = 7");
n_samples_to_skip = 2*4e5;
n_samples_to_read = 4*2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

%%SYNC FAILS BECAUSE OF LOW OSF
disp("TEST 21: idk, SF = 7");
n_samples_to_skip = 2*5.2e5;
n_samples_to_read = 8*2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)
%}
%{
disp("TEST 22: idk, SF = 7");
n_samples_to_skip = 0;
n_samples_to_read = 1e7;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)
%}

%%NOTE FILE SEQUENCE IS WRONG (not 0 2 4...)
%% disp("TEST 23: idk, SF = 7");
%% n_samples_to_skip = 0;
%% n_samples_to_read = 2.5e5;
%% bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)



disp("TEST 24: idk, SF = 7");
n_samples_to_skip = 2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, -0.125, n_samples_to_skip, n_samples_to_read)
%{
disp("TEST 25: idk, SF = 7");
n_samples_to_skip = 2*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 26: idk, SF = 7");
n_samples_to_skip = 3*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 27: idk, SF = 7");
n_samples_to_skip = 4*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 28: idk, SF = 7");
n_samples_to_skip = 5*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 29: idk, SF = 7");
n_samples_to_skip = 6*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 30: idk, SF = 7");
n_samples_to_skip = 7*2*2.9e4;
n_samples_to_read = 2.6e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcon_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)
%}






%{
disp("TEST 23: idk, SF = 7");
n_samples_to_skip = 0;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 24: idk, SF = 7");
n_samples_to_skip = 2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 25: idk, SF = 7");
n_samples_to_skip = 2*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 26: idk, SF = 7");
n_samples_to_skip = 3*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 27: idk, SF = 7");
n_samples_to_skip = 4*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 28: idk, SF = 7");
n_samples_to_skip = 5*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 29: idk, SF = 7");
n_samples_to_skip = 6*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)

disp("TEST 30: idk, SF = 7");
n_samples_to_skip = 7*2*2.9e4;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_cr4_crcoff_1byte_0_2_4_etc_fs1000k.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read)
%}
%{
disp("TEST 31: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 0;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);

disp("TEST 32: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*49e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);

disp("TEST 33: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*85e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);

disp("TEST 34: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*(85e3 + 38e3);
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);

disp("TEST 35: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*(85e3 + 2*38e3);
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);

disp("TEST 36: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*(85e3 + 3*38e3);
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
%}

%{
disp("TEST 37: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 0;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 38: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 39: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 2*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 40: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 3*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 41: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 4*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 42: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 5*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 43: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 6*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 44: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 7*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 45: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 8*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 46: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 9*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;

disp("TEST 47: SF = 7, one hot counting, cr = 4, crc on");
n_samples_to_skip = 10*2*38e3;
n_samples_to_read = 2.5e5;
bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
close all;
%}

%{
%%OK
disp("TEST 48: SF = 7, one hot counting, cr = 4, crc on, 4bytes");
for i = 0:31
  n_samples_to_skip = i*2*38e3;
  n_samples_to_read = 1e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_4bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
  close all;
end;
%}

%{
%%OK
disp("TEST 49: SF = 7, one hot counting, cr = 4, crc on, 2bytes");
for i = 0:15
  n_samples_to_skip = i*2*38e3;
  n_samples_to_read = 2e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_2bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
end;
%}
%{
%%OK
disp("TEST 50: SF = 7, one hot counting, cr = 4, crc on, 3bytes");
for i = 7:30
  n_samples_to_skip = i*2*38e3;
  n_samples_to_read = 2e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_3bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
  close all;
end;
%}

%{
%%OK
disp("TEST 51: SF = 7, one hot counting, cr = 4, crc on, 5bytes");
for i = 1:40
  n_samples_to_skip = i*2*38e3;
  n_samples_to_read = 1e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_5bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
  close all;
end;
%}

%{
%%OK
disp("TEST 52: SF = 7, one hot counting, cr = 4, crc on, 6bytes");
for i = 0:0
  n_samples_to_skip = i*2*46e3;
  n_samples_to_read = 1.5e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_6bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
%%  close all;
end;
%}

%{
%%OK
disp("TEST 53: SF = 7, one hot counting, cr = 4, crc on, 7bytes");
for i = 0:0%%55
  n_samples_to_skip = i*2*46e3;
  n_samples_to_read = 1.6e5;
  bits = receive_from_file("./sample_data/out_sdr_sf7_fs1000k_length_7bytes_onehot_counting.raw", 7, 0x00, 1.0, 0.125, 0, n_samples_to_skip, n_samples_to_read);
  %%close all;
end;
%}
