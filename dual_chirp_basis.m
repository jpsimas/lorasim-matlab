close all;
clear all;

SF = 7;
OSF = 1;
symbol_length = 2^SF*OSF;

chirp_matrix = zeros(symbol_length, 2^SF);
for i = 1:2^SF
  chirp_matrix(:, i) = lora_get_symbol_freq(i-1, SF, OSF);
end;

%%chirp_matrix_inv = inv(chirp_matrix);
%B = zeros(size(chirp_matrix));
%B(:, 1) = -cos(2*pi*(0:(symbol_length - 1))/(symbol_length)).';
%for i = 2:2^SF
%  B(:, i) = B(mod((0:(symbol_length-1)) + OSF, symbol_length) + 1, i-1);
%end;

B = chirp_matrix'*inv(chirp_matrix*chirp_matrix');

size(chirp_matrix)
size(B)

figure;
hold on;
plot(B(:, 1));
plot(chirp_matrix(:, 1));

figure;
hold on;
plot(B(:, 2));
plot(chirp_matrix(:, 2));

figure;
hold on;
plot(B(:, 2^(SF-1)));
plot(chirp_matrix(:, 2^(SF-1)));

x = B'*chirp_matrix;

M = chirp_matrix;
size(chirp_matrix);
for i = 1:size(M, 1)
  M(:, i) = mgorth(M(:, i), M(:, (i + 1):end));
end;

figure;
hold on;
plot(M(:, 1));
figure;
plot(M(:, 2));
figure;
plot(M(:, 3));
figure;
plot(M(:, 2^(SF - 1)));
