clear all;
close all;

pkg load communications;

%%SF to get 8 bit words, so if we use cr = 4 we dont
%%can use them directly as symbols
SF = 10;
CR = 4;

s = lora_get_whitening_sequence(SF, false, true);

s = s(1:100);%%it doesnt need to be so large

s = s(1:((4 + CR)*floor(length(s)/(4 + CR))));

S = gray_encode(s, SF);

%%first 2 groups of 4 symbols are the same, so exclude them for analysis
%%S = S(9:end, :);

C = lora_deinterleave(S, CR, false);

%%sizec = size(C);
%%C = C(sizec(1):-1:1,:)
%%C = C(1:(sizec(1)*sizec(2)));
%%C = C.';

s2 = lora_get_whitening_sequence(SF, true, true);

s2 = s2(1:100);%%it doesnt need to be so large

s2 = s2(1:((4 + CR)*floor(length(s2)/(4 + CR))));

S2 = gray_encode(s2, SF - 2);

%%first 2 groups of 4 symbols are the same, so exclude them for analysis
%%S2 = S2(9:end, :);

C2 = lora_deinterleave(S2, CR, true);

size(C2)

disp("S");
S(1:15, :)
S2(1:15, :)

%%S(1:15,:) == S2(1:15,:)

disp("C");
C(1:15,:)
C2(1:15,:)

C(1:15,:) == C2(1:15,:)

C = C(1:8, :);
C2 = C2(1:8, :);

sizec = size(C);
Cbits = C(1:(sizec(1)*sizec(2)));
w = lfsr_massey2(Cbits);
size(w)

C = C(sizec(1):-1:1,:);
Cbits = C(1:(sizec(1)*sizec(2)));
w = lfsr_massey2(Cbits);
size(w)

%{
sizes = size(S);
Sbits = S(1:(sizes(1)*sizes(2)));

w = lfsr_massey2(Sbits);
size(w)

S = S(sizes(1):-1:1,:);
Sbits = S(1:(sizes(1)*sizes(2)));
w = lfsr_massey2(Sbits);
size(w)
%}
