%% lorasim-matlab
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function bits_out = lora_invert_endianness(bits)
  n_bytes = floor(length(bits)/8);
  bits_out = zeros(size(bits));
  for i = 0:(n_bytes-1)
    bits_out((1:8) + 8*i) = bits((1:8) + 8*(n_bytes - 1 - i));
  end
end

